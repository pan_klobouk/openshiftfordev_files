# Exampleład: Wordpress

The Wordpress application with a database.
We are adding the storage for database and web front.

1. Change the directory
```bash
cd ~/workshop/examples/kubernetes/kustomize/wordpress/
```

2. Download the code from repo
```bash
curl -LO https://k8s.io/examples/application/wordpress/mysql-deployment.yaml
curl -LO https://k8s.io/examples/application/wordpress/wordpress-deployment.yaml
```

3. Create kustomization.yaml file
```bash
cat <<EOF >./kustomization.yaml
secretGenerator:
- name: mysql-pass
  literals:
  - password=YOUR_PASSWORD
resources:
  - mysql-deployment.yaml
  - wordpress-deployment.yaml
EOF
```

4. Create a new namespace test-wordpress
```bash
kubectl create namespace test-wordpress
kubens test-wordpress
```

5. Create an application
```bash
kubectl apply -k ./
```

6. Verify
* Get all secrets
```bash
kubectl get secrets
```
* print mysql password
```bash
kubectl get secrets/mysql-pass-<hash> -o jsonpath="{.data.password}" | base64 --decode
```
* Print all PVC
```bash
kubectl get pvc
```
* print all pods
```bash
kubectl get pods
```
* print all objects for wordpress
```bash
kubectl get all -l app=wordpress
```

7. Go to the www page 
```bash
minikube service wordpress --url
```

![wordpress-kube](images/wordpres_kube.png)


8. Clean
```bash
kubsns -
kubectl delete namespace test-wordpress
```

[go to home](../../../../README.md)
