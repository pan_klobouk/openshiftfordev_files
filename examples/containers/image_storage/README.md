# Example: Images storage

We will check Ubuntu image storage structure:

## Pull images
```bash 
docker pull ubuntu
```

## Check layers in /var/lib/docker/overlay/
```bash 
sudo ls -l /var/lib/docker/overlay/
```
output:
```bash
ls -l /var/lib/docker/overlay2/
total 16
drwx--x--- 3 root root  4096 Dec 11 20:43 fcdf4364f418034a05121d572de774f762e03d694f32ea03bba01ef2752f1e4b
drwx------ 2 root root 12288 Dec 11 20:50 l
```

The image layer directories contain the files unique to that layer as well as hard links
to the data that is shared with lower layers. This allows for efficient use of disk space.

## The container layer

To view the mounts which exist when you use the overlay storage driver with Docker, use the mount command. The output below is truncated for readability.

```bash
docker run -it ubuntu /bin/bash
mount | grep overlay
```

output:
```
overlay on /var/lib/docker/overlay/ec444863a55a.../merged
type overlay (rw,relatime,lowerdir=/var/lib/docker/overlay/55f1e14c361b.../root,
upperdir=/var/lib/docker/overlay/ec444863a55a.../upper,
workdir=/var/lib/docker/overlay/ec444863a55a.../work)
```

The rw on the second line shows that the overlay mount is read-write.

Containers also exist on-disk in the Docker host’s filesystem under /var/lib/docker/overlay/.

If you list a running container’s subdirectory using the ls -l command, three directories
and one file exist:

```bash
ls -l /var/lib/docker/overlay/<directory-of-running-container>

total 16
-rw-r--r-- 1 root root   64 Jun 20 16:39 lower-id
drwxr-xr-x 1 root root 4096 Jun 20 16:39 merged
drwxr-xr-x 4 root root 4096 Jun 20 16:39 upper
drwx------ 3 root root 4096 Jun 20 16:39 work
```

The lower-id file contains the ID of the top layer of the image the container is based on, which is the OverlayFS lowerdir.

```bash
cat /var/lib/docker/overlay/ec444863a55a9f1ca2df72223d459c5d940a721b2288ff86a3f27be28b53be6c/lower-id

55f1e14c361b90570df46371b20ce6d480c434981cbda5fd68c6ff61aa0a5358
```

The upper directory contains the contents of the container’s read-write layer, which corresponds to the OverlayFS upperdir.

The merged directory is the union mount of the lowerdir and upperdir, which comprises the view of the filesystem from within the running container.

The work directory is internal to OverlayFS.

source: https://docs.docker.com/storage/storagedriver/overlayfs-driver/
