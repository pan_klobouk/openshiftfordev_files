# OpenShift for Developers

## Directories:

* presentation -- contain all presentation files
* exercises -- contain all exercises
* examples -- contain examples directories

## Requirements

To run all exercises you need to have the following tools in your workstation:

* docker
* dive - https://github.com/wagoodman/dive
* jq - https://stedolan.github.io/jq/
* nsentry  - https://man7.org/linux/man-pages/man1/nsenter.1.html
* skopeo - https://github.com/containers/skopeo
* python3 docker - https://docker-py.readthedocs.io/en/stable/

* crc - https://www.okd.io/crc/
* openshift-installer and openshift-client
     - https://github.com/openshift/okd/releases
     - https://github.com/okd-project/okd/releases
     - https://github.com/openshift/oc/tags
     - https://access.redhat.com/downloads/content/290

* minikube - https://minikube.sigs.k8s.io/docs/start/
* stern - https://github.com/wercker/stern
* Kube-ps1 - https://github.com/jonmosco/kube-ps1

* s2i:
  - https://github.com/openshift/source-to-image
  - https://dev.to/jromero/creating-an-s2i-builder-for-go-and-a-runtime-image-5d56
  - https://github.com/openshift/source-to-image/blob/master/docs/runtime_image.md
* kompose - https://kompose.io
* tkn - https://tekton.dev/docs/cli/
* kn - https://github.com/knative/client/releases
* crictl - https://github.com/cri-o/cri-o/blob/main/tutorial.md
* kube-bench - https://github.com/aquasecurity/kube-bench
* umoci - https://umo.ci/
* trivy - https://aquasecurity.github.io/trivy/v0.17.2/

## Links

* Devops: 
  - https://martinfowler.com/agile.html
  - https://circleci.com/blog/a-brief-history-of-devops-part-i-waterfall
  - http://agilemanifesto.org/iso/pl/principles.html
* Fluentd - https://www.fluentd.org
* Best Practices Around Production Ready Web Apps with Docker Compose - https://nickjanetakis.com/blog/best-practices-around-production-ready-web-apps-with-docker-compose
* Demystifying-containers - https://github.com/saschagrunert/demystifying-containers
* Enable buildkit - https://docs.docker.com/develop/develop-images/build_enhancements/
* Docker build with secret - https://docs.docker.com/develop/develop-images/build_enhancements/#new-docker-build-secret-information
* Docker alternative:
  - https://www.youtube.com/watch?v=oXog5BrWXeM&ab_channel=BretFisherDockerandDevOps
  - Minikube - https://minikube.sigs.k8s.io/docs/tutorials/docker_desktop_replacement/
  - lima - https://github.com/lima-vm/lima
  - colima - https://github.com/abiosoft/colima
* multi-stage build:
  - https://docs.docker.com/develop/develop-images/multistage-build/
  - https://blog.alexellis.io/mutli-stage-docker-builds/}
  - https://www.docker.com/blog/advanced-dockerfiles-faster-builds-and-smaller-images-using-buildkit-and-multistage-builds/
* Environment variable expose in pod  - https://kubernetes.io/docs/tasks/inject-data-application/environment-variable-expose-pod-information/
* Notary -  https://github.com/notaryproject/notary
* Kubesec - https://kubesec.io/
* Container labels/Annotations - https://github.com/opencontainers/image-spec/blob/main/annotations.md
* FairwindsOps:
  - pluto - https://github.com/FairwindsOps/pluto
  - polaris - https://github.com/FairwindsOps/polaris
  - Goldilocks - https://github.com/FairwindsOps/Goldilocks
* Buildkit:
  - https://docs.docker.com/buildx/working-with-buildx/
  - https://github.com/moby/buildkit
* Minikube:
  - https://minikube.sigs.k8s.io/docs/
  - https://minikube.sigs.k8s.io/docs/handbook/pushing/
* Kubernetes: The Documentary - https://youtu.be/BE77h7dmoQU
* Openshift:
  - Minishift: https://github.com/minishift/minishift/releases
  - MicroShift: https://microshift.io/
  - Unable to delete provisioned services:
    - https://github.com/openshift/origin-web-console/issues/2930
    - https://github.com/openshift/origin/issues/18125
  - Java:
    - https://docs.openshift.com/container-platform/3.11/using_images/s2i_images/java.html
    - https://docs.openshift.com/container-platform/3.11/dev_guide/application_memory_sizing.html
  - audit logs - https://docs.openshift.com/container-platform/3.11/security/monitoring.html#security-monitoring-audit-log
  - s2i:
    - set Dockerfile Path: https://docs.openshift.com/container-platform/3.11/dev_guide/builds/build_strategies.html#dockerfile-path
  - Configuring project creation: https://docs.okd.io/latest/applications/projects/configuring-project-creation.html#configuring-project-creation
  - Configuring alert notifications: https://docs.okd.io/latest/post_installation_configuration/configuring-alert-notifications.html
  - Customizing the web console: https://docs.openshift.com/container-platform/4.9/web_console/customizing-the-web-console.html
  - Backup:
    - https://access.redhat.com/documentation/en-us/openshift_container_platform/4.11/html/backup_and_restore/application-backup-and-restore
    - https://docs.openshift.com/container-platform/4.11/backup_and_restore/control_plane_backup_and_restore/backing-up-etcd.html
    - https://docs.openshift.com/container-platform/4.11/security/certificate_types_descriptions/service-ca-certificates.html
  * RedHat developer sandbox - https://developers.redhat.com/developer-sandbox/get-started
* RedHat doc:
  - https://redhat-scholars.github.io/cloudnative-tutorials/index.html
  - https://redhat-scholars.github.io/openshift-starter-guides/rhs-openshift-starter-guides/4.9/index.html
* Openshift Examples:
  - https://examples.openshift.pub/


### Docker-in-Docker/Testing

* Using Docker-in-Docker for your CI or testing environment? Think twice - http://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/
* Kaniko is a tool to build container images from a Dockerfile, inside a container or Kubernetes cluster - https://github.com/GoogleContainerTools/kaniko
* How to build image in gitlab using kaniko - https://docs.gitlab.com/ee/ci/docker/using_kaniko.html
* Multi-stage Dockerfile for testing - https://docs.docker.com/language/java/run-tests/#multi-stage-dockerfile-for-testing

## HowTo

You need to clone repository to correct directory in your file system, like:

```bash
git clone https://gitlab.com/greenitnet/openshiftfordev_files.git ~/workshop
```

## Examples list

### Containers:

#### [Example: Images layer structure](examples/containers/images/README.md): Images layer structure
#### [Example: Images storage](examples/containers/image_storage/README.md): Images storage

### Kubernetes/OpenShift:

### [Examples kuberentes kustomize - hello_world_flask](examples/openshift/kustomize/hello_world_flask/README.md): hello_world_flask in dev and prod env

## Exercises list

### Containers:
### [Exercise 1](exercises/containers/exercise1/README.md): Using runc to run container
### [Exercise 2](exercises/containers/exercise2/README.md): Run application in container using cri-0
### [Exercise 3](exercises/containers/exercise3/README.md): Container logs
### [Exercise 4](exercises/containers/exercise4/README.md): Debug application
### [Exercise 5](exercises/containers/exercise5/README.md): Run application in container using k8s + cri-o
### [Exercise 6](exercises/containers/exercise6/README.md): Container image from command line
### [Exercise 7](exercises/containers/exercise7/README.md): Container image from file
### [Exercise 8](exercises/containers/exercise8/README.md): Dockerfile building context
### [Exercise 9](exercises/containers/exercise9/README.md): Public remote repository
### [Exercise 10](exercises/containers/exercise10/README.md): Dockerfile multi-stage build
### [Exercise 11](exercises/containers/exercise11/README.md): Deployment Configuration using gitlab-ci.yml
### [Exercise 12](exercises/containers/exercise12/README.md): Docker inspect
### [Exercise 13](exercises/containers/exercise13/README.md): Docker logs
### [Exercise 14](exercises/containers/exercise14/README.md): Docker storage volume
### [Exercise 15](exercises/containers/exercise15/README.md): Docker storage bind
### [Exercise 16](exercises/containers/exercise16/README.md): Docker storage bind overwrite dir
### [Exercise 17](exercises/containers/exercise17/README.md): Docker Containers management
### [Exercise 18](exercises/containers/exercise18/README.md): Container security - compromising secrets
### [Exercise 19](exercises/containers/exercise19/README.md): Container security - Fixing cowsay application

### Openshift:

### [Exercise 1](exercises/openshift/exercise1/README.md): Run CodeReadyContainers
### [Exercise 2](exercises/openshift/exercise2/README.md): Check your cluster
### [Exercise 3](exercises/openshift/exercise3/README.md): Create new project
### [Exercise 4](exercises/openshift/exercise4/README.md): Start app from command line using cli (imperative approach)
### [Exercise 5](exercises/openshift/exercise5/README.md): Show information about pod restarts
### [Exercise 6](exercises/openshift/exercise6/README.md): Add labels
### [Exercise 7](exercises/openshift/exercise7/README.md): Start application from yaml file (declarative approach)
### [Exercise 8](exercises/openshift/exercise8/README.md): Delete resources
### [Exercise 9](exercises/openshift/exercise9/README.md): Get information about all PODs configuration options
### [Exercise 10](exercises/openshift/exercise10/README.md): Check what default values are set for busybox POD
### [Exercise 11](exercises/openshift/exercise11/README.md): Get information about namespace in POD in metadata
### [Exercise 12](exercises/openshift/exercise12/README.md): Get information about horizontalpodautoscalers
### [Exercise 13](exercises/openshift/exercise13/README.md): Liveness and readiness probe
### [Exercise 14](exercises/openshift/exercise14/README.md): Create application based on image
### [Exercise 15](exercises/openshift/exercise15/README.md): Create application based on code git repository
### [Exercise 16](exercises/openshift/exercise16/README.md): Create application based on Dockerfile
### [Exercise 17](exercises/openshift/exercise17/README.md): Create application using binary mode
### [Exercise 18](exercises/openshift/exercise18/README.md): Build image using Source to image tool
### [Exercise 19](exercises/openshift/exercise19/README.md): Build s2i image in OpenShift
### [Exercise 20](exercises/openshift/exercise20/README.md): Overwrite s2i scripts
### [Exercise 21](exercises/openshift/exercise21/README.md): Share files between next builds
### [Exercise 22](exercises/openshift/exercise22/README.md): S2i incremental build java example
### [Exercise 23](exercises/openshift/exercise23/README.md): Build go application using s2i
### [Exercise 24](exercises/openshift/exercise24/README.md): Docker multi-build in OpenShift
### [Exercise 25](exercises/openshift/exercise25/README.md): Change referencePolicy in ImageStream
### [Exercise 26](exercises/openshift/exercise26/README.md): Using labels in deployment
### [Exercise 27](exercises/openshift/exercise27/README.md): Triggers
### [Exercise 28](exercises/openshift/exercise28/README.md): Create DeamonSet
### [Exercise 29](exercises/openshift/exercise29/README.md): Test statefulset application
### [Exercise 30](exercises/openshift/exercise30/README.md): Cronjob with cowsay application
### [Exercise 31](exercises/openshift/exercise31/README.md): Create a POD with a configmap as volume
### [Exercise 32](exercises/openshift/exercise32/README.md): Rolling Strategy
### [Exercise 33](exercises/openshift/exercise33/README.md): Recreate strategy
### [Exercise 34](exercises/openshift/exercise34/README.md): Blue/Green strategy
### [Exercise 35](exercises/openshift/exercise35/README.md): Canary Strategy
### [Exercise 36](exercises/openshift/exercise36/README.md): Update and rollback
### [Exercise 37](exercises/openshift/exercise37/README.md): Application scale
### [Exercise 38](exercises/openshift/exercise38/README.md): Get information about the variable metrics in HorizontalPodAutoscaler
### [Exercise 39](exercises/openshift/exercise39/README.md): Use an HPA based on CPU
### [Exercise 40](exercises/openshift/exercise40/README.md): Use an HPA based on Memory
### [Exercise 41](exercises/openshift/exercise41/README.md): Use an VPA
### [Exercise 42](exercises/openshift/exercise42/README.md): Knative - hello-world
### [Exercise 43](exercises/openshift/exercise43/README.md): Add pod limits as container environment variable
### [Exercise 44](exercises/openshift/exercise44/README.md): Test default limits in project
### [Exercise 45](exercises/openshift/exercise45/README.md): Test quota in project
### [Exercise 46](exercises/openshift/exercise46/README.md): Pod with shared storage between containers
### [Exercise 47](exercises/openshift/exercise47/README.md): Use revers proxy inside a POD
### [Exercise 48](exercises/openshift/exercise48/README.md): Application with init container
### [Exercise 49](exercises/openshift/exercise49/README.md): Create ClusterIP service for nginx application
### [Exercise 50](exercises/openshift/exercise50/README.md): Create headless service for nginx application
### [Exercise 51](exercises/openshift/exercise51/README.md): Service my-service -> www.nobleprog.pl
### [Exercise 52](exercises/openshift/exercise52/README.md): Checking LB for my-application service

### [Exercise 53](exercises/openshift/exercise53/README.md): Create route with ssl for my-application
### [Exercise 54](exercises/openshift/exercise54/README.md): Route with sticky session
### [Exercise 55](exercises/openshift/exercise55/README.md): Route restrictions
### [Exercise 56](exercises/openshift/exercise56/README.md): Route HTTP rate limiting

### [Exercise 57](exercises/openshift/exercise57/README.md): Service mesh - Create recommendation applicaion
### [Exercise 58](exercises/openshift/exercise58/README.md): Service mesh - deployment blue-green
### [Exercise 59](exercises/openshift/exercise59/README.md): Service mesh - deployment canary
### [Exercise 60](exercises/openshift/exercise60/README.md): Service mesh - deployment ab
### [Exercise 61](exercises/openshift/exercise61/README.md): Service mesh - deployment shadow (Dark Launch)
### [Exercise 62](exercises/openshift/exercise62/README.md): Test emptydir
### [Exercise 63](exercises/openshift/exercise63/README.md): Using the PVC
### [Exercise 64](exercises/openshift/exercise64/README.md): LAMP application
### [Exercise 65](exercises/openshift/exercise65/README.md): Template
### [Exercise 66](exercises/openshift/exercise66/README.md): Tekton - Gretings task
### [Exercise 67](exercises/openshift/exercise67/README.md): Applicaion with pod anti-affinity settings
### [Exercise 68](exercises/openshift/exercise68/README.md): Create a new user
### [Exercise 69](exercises/openshift/exercise69/README.md): Create rule for user test-user
### [Exercise 70](exercises/openshift/exercise70/README.md): Start pod with set UID
### [Exercise 71](exercises/openshift/exercise71/README.md): Turn off capabilities
### [Exercise 72](exercises/openshift/exercise72/README.md): Security Context Constraints
### [Exercise 73](exercises/openshift/exercise73/README.md): Create a secret
### [Exercise 74](exercises/openshift/exercise74/README.md): Network policy - restricted access from pod
### [Exercise 75](exercises/openshift/exercise75/README.md): Application logs processing
### [Exercise 76](exercises/openshift/exercise76/README.md): DT - Hot R.O.D. - Rides on Demand
### [Exercise 77](exercises/openshift/exercise77/README.md): Gitlab pipelines
