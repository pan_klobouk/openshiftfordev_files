# Exercise 49: Create ClusterIP service for nginx application


## Create new project 
```bash
oc new-project $(oc whoami)-network
```

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise49/
```

2. Create a service
```bash
oc apply -f svc-my-app.yaml
```

4. Create an application
```bash
oc apply -f my-app.yaml
```


5. Start a load-generator
```bash
oc run -i --tty load-generator \
  --rm \
  --image=busybox \
  --restart=Never \
  -- /bin/sh -c "while sleep 0.2;\
   do wget -q -O /dev/null http://my-app;\
   done"
```

6. In the second terminal watch logs
```bash
oc logs -f my-app-<tab>
```
or watch logs from all pods:
```bash
stern my-app
```

7. Stop ```load-generator``` application.

[go to home](../../../README.md)

[go to next](../exercise50/README.md)
