# Exercise 35: Canary Strategy

1. Create Cat of the Day application 
```bash 
oc new-app \
 --name='cats' \
 -l app='cats'\
 php:7.3~https://github.com/devops-with-openshift/cotd.git \
 -e SELECTOR=cats
```

2. Expose service 
```bash 
oc expose service cats --name=cats -l app='cats' --hostname=cats.crc-okd.mnieto.pl
```

3. Check cats application 
```bash
curl http://$(oc get route/cats -o jsonpath='{.status.ingress[0].host}')/
```
```bash
firefox  http://$(oc get route/cats -o jsonpath='{.status.ingress[0].host}')/
```

4. Create City of the Day application
```bash 
oc new-app \
  --name='city' \
  -l app='city' \
  php:7.3~https://github.com/devops-with-openshift/cotd.git \
  -e SELECTOR=cities
```

5. Expose service 
```bash 
oc expose service city --name=city -l app='city' --hostname=city.crc-okd.mnieto.pl
```

6. Check city application: 
```bash
curl http://$(oc get route/city -o jsonpath='{.status.ingress[0].host}')/
```
```bash
firefox  http://$(oc get route/city -o jsonpath='{.status.ingress[0].host}')/
```

7. Check route 
```bash 
oc get routes
```

8. Create new route 
```bash 
oc expose service cats --name='ab' --hostname=ab.crc-okd.mnieto.pl
```
set annotate:
```bash
oc annotate route/ab haproxy.router.openshift.io/balance=roundrobin
oc annotate route/ab haproxy.router.openshift.io/disable_cookies='true'
```

Add new ```city``` backend to route woth weight=0:
```bash 
oc set route-backends ab cats=100 city=0
```

7. Check traffic 
```bash 
SITE=$(oc get route/ab -o jsonpath='{.status.ingress[0].host}')
for i in {1..10}; \
  do curl -s http://${SITE}/item.php | grep "data/images/.*/.*\.jpg" | awk '{print $5}'; \
  done
```

8. In second terminal increase city % in route +10% and check traffic in first terminal
```bash 
oc set route-backends ab --adjust city=+10%
```
```bash 
oc get route 
```

9. In second terminal increase city % in route +40% and check traffic in first terminal
```bash 
oc set route-backends ab --adjust city=+40%
```
```bash
oc get route 
```


10. Clean 
```bash 
oc delete all -l app=cats
```
```bash
oc delete all -l app=city
```
```bash
oc delete all -l app=ab
```
[go to home](../../../README.md)

[go to next](../exercise36/README.md)
