# Exercise 38: Get information about the variable metrics in HorizontalPodAutoscaler

```bash
oc explain hpa.spec
```


[go to home](../../../README.md)

[go to next](../exercise39/README.md)
