# Exercise 3: Create new project

1. Login to cluster over [web UI](https://console-openshift-console.apps-crc.testing/dashboards)  and than create new project
```bash
crc console
```
![Create Project](images/okd_create_project.png)

2. Create new application using "Developer Catalog" (Add -> Developer Catalog  -> All services -> Templates -> php)

![Create applicaion](images/okd_new_project.png)
![New applicaion](images/okd_new_app.png)

3. Check:

- Build
- Pods
- Services

4. Check all objects in your new project from cli:
```bash
oc project <my new project>
oc get all 
```
or
```bash
oc get all -n <my new project>
```

[go to home](../../../README.md)

[go to next](../exercise4/README.md)
