# Exercise 63: Using the PVC

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise63/
```

2. Create a PVC
```bash
oc apply -f pv-clame.yaml
```

3. Check
* PVC
```bash
oc get pvc
```
```bash
oc describe pvc task-pv-claim
```
* PV
---
> **Warning**
>
> You need cluster-admin rights !!
---

```bash
oc login -u kubeadmin
```

```bash
$ oc get pv
```
```bash
$ oc describe pv
```

4. Create a Pod with PVC
```bash
oc apply -f pv-pod.yaml
```
```bash
oc describe pod task-pv-pod
```

5. Clean
```bash
oc delete -f pv-pod.yaml
```
```bash
oc delete -f pv-clame.yaml
```

[go to home](../../../README.md)

[go to next](../exercise64/README.md)
