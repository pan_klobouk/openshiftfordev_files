# Exercise 2: Check your cluster 

1. Check oc help 
```bash 
oc --help
```

2. Check status 
```bash 
oc status 
oc status --help
```

3. Check projects 
```bash 
oc project 
oc projects
oc get projects 
```

4. Check user information 
```bash 
oc whoami 
oc whoami --help
```

5. Check help for create new application 
```bash 
oc new-app --help
```

6. Check help for new build 
```bash 
oc new-build --help
```

7. Check help for new project
```bash 
oc new-project --help
```

[go to home](../../../README.md)

[go to next](../exercise3/README.md)
