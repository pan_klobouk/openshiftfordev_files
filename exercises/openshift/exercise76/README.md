# Exercise 76: DT - Hot R.O.D. - Rides on Demand

1. Start jaeger in docker
```bash
docker run -d\
 --name jaeger\
 -p 6831:6831/udp \
 -p 16686:16686 \
 jaegertracing/all-in-one:latest
```
* Open Jaeger application on [http://127.0.0.1:16686](http://127.0.0.1:16686/)

![Jaeger](images/jaeger_app.png)

2. Clone repo with HotROD application:
```bash
git clone https://github.com/jaegertracing/jaeger.git ~/jaeger
cd ~/jaeger
```
3. Build and run application:
```bash
go run ./examples/hotrod/main.go all
```

4. Check application on [http://127.0.0.1:8080](http://127.0.0.1:8080) and click on one of the client buttons:

![HotROD](images/hotrod_app.png)

5. Check in Jaeger DAG:

![Jaeger DAG](images/jaeger_dag.png)

6. Check Data Flow:

![Data Flow](images/jaeger_flow1.png)
![Data Flow for /dispatch](images/jaeger_flow2.png)

Connection flow:

client           -> /dispatcher (service frontend)

service frontend -> /customer -> run SQL query

service frontend -> RPC request to the driver service -> call to Redis

service frontend -> /route

service frontend -> return results to client

7. Check tags and logs of any spans:

![Span logs & tags](images/jaeger_span.png)

8. Create load by click on one of the buttons repeatedly (and quickly)

![Load](images/jaeger_load.png)

9. In the last request SQL query take 1.4s

![MySQL bottleneck](images/jaeger_mysql_long.png)

10. Stop application and check files:
```bash
vim examples/hotrod/services/customer/database.go +86
```
* Change *MySQLGetDelay* and *MySQLMutexDisabled*:

```bash
--- a/examples/hotrod/services/config/config.go
+++ b/examples/hotrod/services/config/config.go
@@ -30,14 +30,14 @@ var (

        // MySQLGetDelay is how long retrieving a customer record takes.
        // Using large value mostly because I cannot click the button fast enough to cause a queue.
-       MySQLGetDelay = 300 * time.Millisecond
+       MySQLGetDelay = 1 * time.Millisecond

        // MySQLGetDelayStdDev is standard deviation
        MySQLGetDelayStdDev = MySQLGetDelay / 10

        // MySQLMutexDisabled controls whether there is a mutex guarding db query execution.
        // When not disabled it simulates a misconfigured connection pool of size 1.
-       MySQLMutexDisabled = false
+       MySQLMutexDisabled = true

```

11. Start application and check connection time again :
```bash
go run ./examples/hotrod/main.go all
```

![MySQL fix](images/jaeger_mysql_fix.png)


12. What else we can see (sources of latency):
* issue with redis (timeouts)
* issue with parallel request to /route

more info on blog [Take OpenTracing for a HotROD ride](https://medium.com/opentracing/take-opentracing-for-a-hotrod-ride-f6e3141f7941)

13. Cleaning:
* stop go application
* stop jaeger in docker:   
```bash
docker stop jaeger
```

[go to home](../../../README.md)

[go to next](../exercise77/README.md)
