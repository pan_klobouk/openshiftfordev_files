# Exercise 17: Create application using binary mode 

1. Create application using code git repo 
```bash 
oc new-app \
  --name='php-test' \
  -l name='php-test' \
  php~https://gitlab.com/greenitnet/php-docker-test.git#s2i
```

2. Expose application 
```bash 
oc expose svc/php-test --path='/www'
```

3.  Add CRC ip address and application host to /etc/hosts (as root)
```bash
echo "192.168.130.11 $(oc get route/php-test -o jsonpath='{.status.ingress[0].host}')"
```

4. Check application 
```bash 
curl http://$(oc get route/php-test -o jsonpath='{.status.ingress[0].host}')/www/
```

5. Clone code repository to your desktop and change branch 
```bash 
git clone https://gitlab.com/greenitnet/php-docker-test.git && cd php-docker-test
git checkout s2i
```

6. Change code application e.g. change background image in www/index.html 
- change body to color blue: 
```bash 
<body> -> <body style="background-color:blue">
```

7. Start new build from code in your desktop 
```bash 
oc start-build php-test --from-dir="." --follow
```

8. Check application 
```bash 
curl http://$(oc get route/php-test -o jsonpath='{.status.ingress[0].host}')/www/
```

9. Start new build using default configuration 
```bash 
oc start-build php-test
```

10. Wait for new applicaion pod and check application 
```bash 
curl http://$(oc get route/php-test -o jsonpath='{.status.ingress[0].host}')/www/
```

11. Clean 
```bash 
oc delete all -l name='php-test'
``` 

[go to home](../../../README.md)

[go to next](../exercise18/README.md)
