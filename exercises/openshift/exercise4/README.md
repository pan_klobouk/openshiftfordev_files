# Exercise 4: Start app from command line using cli (imperative approach)

## Create Pod

* Check options for run command 
```bash
oc run -h
```

* Create Pod using image busybox with command sleep
```bash
oc run \
  busybox-cli \
  --image=registry.gitlab.com/greenitnet/images/busybox:latest \
  --image-pull-policy=IfNotPresent \
  sleep 3600
```

* Check objects in current namespace
```bash
oc get all
```

* Check objects in all namespaces  
```bash
oc get all --all-namespaces
```
or
```bash 
oc get all -A
```


[go to home](../../../README.md)

[go to next](../exercise5/README.md)
