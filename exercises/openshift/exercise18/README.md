# Exercise 18: Build image using Source to image tool

1. Clone s2i repository
```bash
git clone https://github.com/openshift/source-to-image ~/source-to-image
cd ~/source-to-image/examples/nginx-centos7
```

2. Build builder image
```bash
docker build -t nginx-centos7 .
```

3. Build image using s2i builder image
```bash
s2i build test/test-app nginx-centos7 nginx-centos7-app
```

4. Check image with different input data:
Depends on source we have two action define in file file s2i/bin/assemble:
```bash
if [ -f /tmp/src/nginx.conf ]; then
  mv /tmp/src/nginx.conf /etc/nginx/nginx.conf
fi
if [ "$(ls -A /tmp/src)" ]; then
  mv /tmp/src/* /usr/share/nginx/html/
fi
```
![test-app](images/test-app.png)



Build and check new image:
```bash
docker run --rm -d -p 8080:8080 --name s2i-test nginx-centos7-app
```

Check applicaion:
```bash
curl -v localhost:8080
```

Stop container: 
```bash
docker stop s2i-test
```

5. Build, run  and check new image using different data
```bash
s2i build test/test-app-redirect nginx-centos7 nginx-centos7-redirect
```

Run: 
```bash
docker run --rm -d -p 8080:8080 --name=s2i-test nginx-centos7-redirect
```

Check: 
```bash
curl -v localhost:8080
```

Stop:
```bash
docker stop s2i-test
```

[go to home](../../../README.md)

[go to next](../exercise19/README.md)
