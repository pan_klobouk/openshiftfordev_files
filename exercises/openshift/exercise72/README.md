# Exercise 72: Security Context Constraints

doc: [manage_scc](https://docs.openshift.com/container-platform/3.11/admin_guide/manage_scc.html)

---
> **Warning**
>
> You must have cluster-admin privileges to manage SCCs.
---

1. Change the directory
```bash 
cd ~/workshop/exercises/openshift/exercise72/
```

2. Create application test-apache-json
```bash
for file in `ls test-apache-json/*.yaml`; do oc apply -f $file; done
```
```bash
oc get all -l app=test-apache-json
```

3. Check current user id
```bash
oc rsh dc/test-apache-json ps aux
```
output
```
USER        PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
1000120+      1  0.4  0.0  83316 14708 ?        Ss   18:06   0:00 apache2 -DFORE
1000120+     18  0.0  0.0  83340  5440 ?        S    18:06   0:00 apache2 -DFORE
1000120+     19  0.0  0.0  83340  5440 ?        S    18:06   0:00 apache2 -DFORE
1000120+     20  0.0  0.0  83340  5440 ?        S    18:06   0:00 apache2 -DFORE
1000120+     21  0.0  0.0  83340  5440 ?        S    18:06   0:00 apache2 -DFORE
1000120+     22  0.0  0.0  83340  5440 ?        S    18:06   0:00 apache2 -DFORE
1000120+     23  0.0  0.0   7628  1404 ?        Rs+  18:07   0:00 ps aux
```

4. Create serviceaccount
```bash
oc create sa sa-with-wwwdata
```

5. Show all Security Context Constraints (SCC)
```bash
oc get scc
```
output
```
NAME                 PRIV      CAPS      SELINUX     RUNASUSER          FSGROUP     SUPGROUP    PRIORITY   READONLYROOTFS   VOLUMES
anyuid               false     []        MustRunAs   RunAsAny           RunAsAny    RunAsAny    10         false            [configMap downwardAPI emptyDir persistentVolumeClaim projected secret]
hostaccess           false     []        MustRunAs   MustRunAsRange     MustRunAs   RunAsAny    <none>     false            [configMap downwardAPI emptyDir hostPath persistentVolumeClaim projected secret]
hostmount-anyuid     false     []        MustRunAs   RunAsAny           RunAsAny    RunAsAny    <none>     false            [configMap downwardAPI emptyDir hostPath nfs persistentVolumeClaim projected secret]
hostnetwork          false     []        MustRunAs   MustRunAsRange     MustRunAs   MustRunAs   <none>     false            [configMap downwardAPI emptyDir persistentVolumeClaim projected secret]
kube-state-metrics   false     []        RunAsAny    RunAsAny           RunAsAny    RunAsAny    <none>     false            [*]
node-exporter        false     []        RunAsAny    RunAsAny           RunAsAny    RunAsAny    <none>     false            [*]
nonroot              false     []        MustRunAs   MustRunAsNonRoot   RunAsAny    RunAsAny    <none>     false            [configMap downwardAPI emptyDir persistentVolumeClaim projected secret]
privileged           true      [*]       RunAsAny    RunAsAny           RunAsAny    RunAsAny    <none>     false            [*]
restricted           false     []        MustRunAs   MustRunAsRange     MustRunAs   RunAsAny    <none>     false            [configMap downwardAPI emptyDir persistentVolumeClaim projected secret]
restricted-sysctls   false     []        MustRunAs   MustRunAsRange     MustRunAs   RunAsAny    <none>     false            [configMap downwardAPI emptyDir persistentVolumeClaim projected secret
```

6. Show *anyuid* details:
```bash
oc get scc/anyuid -o yaml
```

7. Add serviceaccount to scc anyuid:
```bash
oc adm policy add-scc-to-user anyuid system:serviceaccount:$(oc project -q):sa-with-wwwdata
```
```bash
oc get scc/anyuid -o yaml
```


8. Update DeploymentConfig by adding :
```diff
spec:
  template:
    spec:
+      securityContext:
+        runAsUser: 3000
+      serviceAccount: sa-with-wwwdata
+      serviceAccountName: sa-with-wwwdata
```
or
```bash
oc apply -f test-apache-json/dc.yaml_corrected
```

9. Check user id inside pod:
```bash
oc rsh dc/test-apache-json ps aux
```
output
```
USER        PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
3000          1  0.1  0.0  83316 14708 ?        Ss   18:08   0:00 apache2 -DFORE
3000         18  0.0  0.0  83340  5444 ?        S    18:08   0:00 apache2 -DFORE
3000         19  0.0  0.0  83340  5444 ?        S    18:08   0:00 apache2 -DFORE
3000         20  0.0  0.0  83340  5444 ?        S    18:08   0:00 apache2 -DFORE
3000         21  0.0  0.0  83340  5444 ?        S    18:08   0:00 apache2 -DFORE
3000         22  0.0  0.0  83340  5444 ?        S    18:08   0:00 apache2 -DFORE
3000         30  0.0  0.0   7628  1408 ?        Rs+  18:09   0:00 ps aux
```

[go to home](../../../README.md)

[go to next](../exercise73/README.md)
