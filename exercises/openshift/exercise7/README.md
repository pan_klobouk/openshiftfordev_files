# Exercise 7: Start application from yaml file (declarative approach)

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise7/
```
2. Create Pod from file
```bash
oc create -f busybox.yaml
```

3. Check pods 
```bash
oc get pods
```

4. Check pod labels
```bash 
oc get pod --show-labels
```

[go to home](../../../README.md)

[go to next](../exercise8/README.md)
