# Exercise 37: Application scale


## Change your project back to one with my-application (application in go)
```bash 
oc project _your_old_project_
```


The are two options to scale the deployment 
*  Scale by edit deployment and increase replicas number: 
```bash
oc edit deploymentconfigs my-application
```

* Scale by command line
```bash
oc scale dc my-application --replicas=3
```

[go to home](../../../README.md)

[go to next](../exercise38/README.md)
