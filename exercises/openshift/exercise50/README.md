# Exercise 50: Create headless service for nginx application

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise50/
```

2. Create a headless service
```bash
oc apply -f svc-headless-my-app.yaml
```

3. Create a Pod with dns tools
```bash
oc run dnsutils \
  --image=registry.gitlab.com/greenitnet/dnsutils:latest \
  -- sleep 600
```

4. Check return dns values for services my-app and my-app-headless
```bash
oc exec dnsutils -- dig my-app-headless A +search +short
```
```bash
oc exec dnsutils -- dig my-app A +search +short
```

5. Scale the pod to 2 replicas
```
oc scale deployment my-app --replicas=2
```
6. Check number of pods:
```bash
oc get pods
```

7. Check return dns values for services my-application and my-application-headless
```bash
oc exec dnsutils -- dig my-app-headless A +search +short
```
```bash
oc exec dnsutils -- dig my-app A +search +short
```

[go to home](../../../README.md)

[go to next](../exercise51/README.md)
