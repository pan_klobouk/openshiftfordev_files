# Exercise 21: Share files between next builds

1. Build application with option --incremental=false from example/nginx-centos-7 of source-to-image repository.  
```bash 
cd ~/source-to-image/examples/nginx-centos7
```

build: 
```bash
s2i build test/test-app \
  nginx-centos7 \
  nginx-centos7-app \
  --incremental=false \
  --loglevel 1
```

2. Run application and check saved file: 
```bash 
docker run --rm nginx-centos7-app ls /etc/nginx/artifact.txt
```

3. Build application with option --incremental=true
```bash 
s2i build test/test-app \
  nginx-centos7 \
  nginx-centos7-app \
  --incremental=true \
  --loglevel 1
```

4. Run application and check save file: 
```bash 
docker run --rm nginx-centos7-app ls /etc/nginx/artifact.txt
```

[go to home](../../../README.md)

[go to next](../exercise22/README.md)
