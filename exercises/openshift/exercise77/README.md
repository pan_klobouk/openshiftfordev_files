# Exercise 77: Gitlab pipelines

## Web hook

1. Fork repository gitlab.com/greenitnet/php-docker-test and set as public
2. Build application using web hook:
- Build application:
```bash
oc new-app https://gitlab.com/<yout repository>/php-docker-test.git \
  -l app=test-apache-json \
  -e multiplier=100 \
  --name=test-apache-json
```

- Expose and check application
```bash
oc expose svc test-apache-json --port=8080
```
```bash
curl http://$(oc get route/test-apache-json -o jsonpath='{.status.ingress[0].host}')/randtest.php
```

3. Get web hook url from build config:
![webhook](images/webhook.png)

4. Set web hook in your gitlab repository
![gitlab-webhook](images/gitlab-webhook.png)

5. Change anything in index.html, commit and push to master branch (you can do it on gitlab website)

6. Check your application
```bash
curl http://$(oc get route/test-apache-json -o jsonpath='{.status.ingress[0].host}')/randtest.php
```

## Use serviceaccount to push changes to OpenShift

1. Create Service account and get token
```bash
oc create sa gitlab-push
```
```bash
oc describe secret gitlab-push-token-<tab> -o yaml
```

2. Add edit role for create/build/remove app
```bash
oc policy add-role-to-user edit -z gitlab-push
```

3. Add token to your repository (Settings -> CI/CD -> Variables)
![gitlab-variables](images/gitlab-variables.png)

4. Update .gitlab-ci.yml to your repository
```bash
cat ~/workshop/exercises/openshift/exercise78/gitlab-ci-start-build.yml
```

5. Commit changes and check your application

5. Export all objects to yaml files and save them in okd-conf directory in your repository
or copy them from:
```bash
git clone git@gitlab.com:<yout repository>/php-docker-test.git ~/php-docker-test
cp ~/workshop/exercises/openshift/exercise34/okd-conf ~/php-docker-test/
```
> **If you copy files from exercise, than You need to correct project name in bc.yaml file.**


6. Delete all object related to app=test-apache-json
```bash
oc delete all -l app=test-apache-json
```

7. Add new gitlab-ci.yml (gitlab-ci-all.yml) and commit changes
```bash
cp ~/workshop/exercises/openshift/exercise34/gitlab-ci-all.yml ~/php-docker-test/.gitlab-ci.yml
git add <all you need to add>
git commit -m ....
git push
```
8. Check pipelines in gitlab and project in okd.

[go to home](../../../README.md)
