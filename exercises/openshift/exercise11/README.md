# Exercise 11: Get information about namespace in POD in metadata

```bash 
oc explain pod.metadata
```

[go to home](../../../README.md)

[go to next](../exercise12/README.md)
