# Exercise 15: Create application based on code git repository 

1. Create application 
```bash 
oc new-build \
   --name='cats' \
   -l name='cats'\
   https://github.com/devops-with-openshift/cotd.git \
   -e SELECTOR=cats
```

2. Check logs from build 
```bash
oc logs -f bc/cats
```

or start build with higher log level and follow option: 
```bash 
oc start-build --build-loglevel=5 cats --follow
```

3. Check objects in project 
```bash 
oc get all
```

4. Clean 
```bash 
oc delete all -l name=cats
```


[go to home](../../../README.md)

[go to next](../exercise16/README.md)
