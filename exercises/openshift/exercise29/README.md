# Exercise 29: Test statefulset application

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise29/
```

2. Create StateFulset
```bash
oc apply -f my-app-statefulset.yaml
```

3. Check a status
```bash
oc get sts
```
```bash
oc get all
```

4. Check PersistentVolumeClaim
```bash
oc get pvc
```

5. Clean
Delete statefuleset 
```bash
oc delete -f my-app-statefulset.yaml
```

Delete PVC
```bash
oc delete pvc -l app=nginx
```

[go to home](../../../README.md)

[go to next](../exercise30/README.md)
