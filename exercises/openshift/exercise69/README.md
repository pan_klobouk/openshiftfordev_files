# Exercise 69: Create rule for user test-user

---
> **Warning**
>
> You need cluster-admin rights !!
>
---



1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise69/
```

2. Apply the role and role-binding
```bash
oc apply -f role.yaml
oc apply -f role-binding.yaml
```

3. Check roles and role-bindings  
```bash
oc get roles
oc get rolebindings
```

4. login as test-user
```bash
oc logout 
oc login -u test-user
oc get pods 
oc get all 
oc new-project test
```

5. Check what can i do 
```bash 
oc auth can-i --list --namespace default 
```


6. Change to kubeadmin user
```bash
oc logout 
oc login -u kubeadmin
```

7. Print default cluster roles:
```bash
oc get clusterroles.rbac.authorization.k8s.io
oc get clusterroles.rbac.authorization.k8s.io view -o yaml
```

8. Check oc adm policy command 
```bash 
oc adm policy -h
```


[go to home](../../../README.md)

[go to next](../exercise70/README.md)
