# Exercise 60: Service mesh - deployment ab

1. Create VS for main recommendation app
```bash
oc create -f istiofiles/virtual-service-recommendation-v1.yml
```

2. Create  VS only for safari users
```bash
oc replace -f istiofiles/virtual-service-safari-recommendation-v2.yml
``` 

3. Check
```bash
export GATEWAY_URL=$(oc get route istio-ingressgateway -n istio-system -o=jsonpath="{.spec.host}")
while true;\
do \
  curl ${GATEWAY_URL}/customer;\
  sleep 0.2;\
done
```

In second terminal:
```bash
export GATEWAY_URL=$(oc get route istio-ingressgateway -n istio-system -o=jsonpath="{.spec.host}")
while true;\
do \
  curl -A Safari ${GATEWAY_URL}/customer;\
  sleep 0.2;\
done
```

4. Clean
```bash
oc delete -f istiofiles/virtual-service-safari-recommendation-v2.yml
```

[go to home](../../../README.md)

[go to next](../exercise61/README.md)

---
source: https://redhat-scholars.github.io/istio-tutorial/
