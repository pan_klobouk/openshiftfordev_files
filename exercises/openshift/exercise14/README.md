# Exercise 14: Create application based on image

1. Create application 
```bash 
oc new-app \
  --name='php-test-image' \
  -l name='php-test-image' \
  registry.gitlab.com/greenitnet/php-docker-test:latest
```

2. Check application 
```bash 
oc get all --show-labels
```

3. Create route 
```bash 
oc expose svc/php-test-image --port=8080
```

4. Print route
```bash
oc get route
```

5.  Add CRC ip address and application host to /etc/hosts (as root)
```bash
echo "192.168.130.11 $(oc get route/php-test-image -o jsonpath='{.status.ingress[0].host}')"
```

6. Check application 
```
curl -v http://$(oc get route/php-test-image -o jsonpath='{.status.ingress[0].host}')/randtest.php
```

7. Clean 
```bash 
oc delete all -l name=php-test-image
```

[go to home](../../../README.md)

[go to next](../exercise15/README.md)
