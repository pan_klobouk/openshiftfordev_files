# Exercise 52: Checking LB for my-application service

1. Login to CRC and switch to roor account
```bash
ssh -i ~/.crc/machines/crc/id_ecdsa core@192.168.130.11
sudo -i
```

2. Check iptables rules for my-application service:
```bash
iptables-save | grep grep my-app:80-tcp
```

```bash
-A KUBE-SVC-RWKGALQGOTQYJIMZ -d 10.217.5.81/32 ! -i tun0 -p tcp -m comment --comment "test-mc/my-app:80-tcp cluster IP" -m tcp --dport 80 -j KUBE-MARK-MASQ
-A KUBE-SVC-RWKGALQGOTQYJIMZ -m comment --comment "test-mc/my-app:80-tcp" -m statistic --mode random --probability 0.25000000000 -j KUBE-SEP-SZHSU7JR3HCWHN4B
-A KUBE-SVC-RWKGALQGOTQYJIMZ -m comment --comment "test-mc/my-app:80-tcp" -m statistic --mode random --probability 0.33333333349 -j KUBE-SEP-BHXN4XH5HXVS65ZK
-A KUBE-SVC-RWKGALQGOTQYJIMZ -m comment --comment "test-mc/my-app:80-tcp" -m statistic --mode random --probability 0.50000000000 -j KUBE-SEP-KLERT7UJIWXVN3WF
-A KUBE-SVC-RWKGALQGOTQYJIMZ -m comment --comment "test-mc/my-app:80-tcp" -j KUBE-SEP-URSV3KPUUZSTT2VW
-A KUBE-SEP-SZHSU7JR3HCWHN4B -s 10.217.0.235/32 -m comment --comment "test-mc/my-app:80-tcp" -j KUBE-MARK-MASQ
-A KUBE-SEP-SZHSU7JR3HCWHN4B -p tcp -m comment --comment "test-mc/my-app:80-tcp" -m tcp -j DNAT --to-destination 10.217.0.235:8080
-A KUBE-SEP-BHXN4XH5HXVS65ZK -s 10.217.0.236/32 -m comment --comment "test-mc/my-app:80-tcp" -j KUBE-MARK-MASQ
-A KUBE-SEP-BHXN4XH5HXVS65ZK -p tcp -m comment --comment "test-mc/my-app:80-tcp" -m tcp -j DNAT --to-destination 10.217.0.236:8080
-A KUBE-SEP-URSV3KPUUZSTT2VW -s 10.217.0.238/32 -m comment --comment "test-mc/my-app:80-tcp" -j KUBE-MARK-MASQ
-A KUBE-SEP-URSV3KPUUZSTT2VW -p tcp -m comment --comment "test-mc/my-app:80-tcp" -m tcp -j DNAT --to-destination 10.217.0.238:8080
```

3. Check pods ip addresses:
```bash
oc get pods -o wide
```

4. Clean test-network namespace
```bash
oc delete project $(oc whoami)-network
```

[go to home](../../../README.md)

[go to next](../exercise53/README.md)
