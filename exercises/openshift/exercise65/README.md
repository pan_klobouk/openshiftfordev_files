# Exercise 65: Template

1. Download template
```bash
mkdir ~/template && cd ~/template
```
```bash
curl \
https://gitlab.com/greenitnet/go-scratch-template/-/raw/master/go-scratch-template.yaml \
 -o go-scratch-template.yaml
```

* add hostname to route:
```diff  
   metadata:
     name: ${NAME}-my-application
   spec:
-    host:
+    host: ${NAME}-my-application.crc-okd.mnieto.pl
     port:
       targetPort: 8080-tcp
     to:
```


2. Add template to our project
```bash 
oc apply -f go-scratch-template.yaml
```

3. Check parameters
```bash 
oc process --parameters go-scratch
```

4. Create application from web UI  (Add -> Developer Catalog -> All services -> Templates -> Simple Go HTTP Server)

![Simple Go HTTP server](images/simple_go_http_server.png)

5. Delete project
```bash
oc delete all -l  template=go-scratch
```

[go to home](../../../README.md)

[go to next](../exercise66/README.md)
