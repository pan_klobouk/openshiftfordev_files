#  Exercise 41: VPA

## Install VPA
1. Go to `Operators` → `OperatorHub`
2. Choose VerticalPodAutoscaler from the list of available Operators, and click Install.
![VPA](images/openshift_vertical_pod_autoscaler.png)
3. On the Install Operator page, ensure that the Operator recommended namespace option is selected.
4. Click Install.
5. Verify the installation in the OpenShift Container Platform CLI using the following command:
```bash
oc get all -n openshift-vertical-pod-autoscaler
```
or by checks Pods in project `openshift-vertical-pod-autoscaler` project in Web GUI
## Test VPA:

1. Clone code
```bash
git clone https://github.com/kubernetes/autoscaler.git ~/autoscaler
cd ~/autoscaler/vertical-pod-autoscaler/
````

2. Create new project:
```bash
oc new-project test-vpa
```

3. Create application:
Correct yaml file:
```diff
diff --git a/vertical-pod-autoscaler/examples/hamster.yaml b/vertical-pod-autoscaler/examples/hamster.yaml
index a4928605a..29b74b498 100644
--- a/vertical-pod-autoscaler/examples/hamster.yaml
+++ b/vertical-pod-autoscaler/examples/hamster.yaml
@@ -44,9 +44,6 @@ spec:
       labels:
         app: hamster
     spec:
-      securityContext:
-        runAsNonRoot: true
-        runAsUser: 65534 # nobody
       containers:
         - name: hamster
           image: k8s.gcr.io/ubuntu-slim:0.1
```

Create VPA and application: 
```bash
oc apply -f examples/hamster.yaml
```

4. Check pods
```bash
watch -n 1 -d "oc get pods"
```
or
```bash
oc get pods -w
```
in second terminal check vpa:
```bash
oc get vpa hamster-vpa
```
```bash
oc describe vpa hamster-vpa"
```

Wait a few minutes. 


5. Check pods and deployment  configuration after pod restart: : 
```bash
oc describe pod $(kubectl get pod -l app=hamster | grep Running | head -n 1 | awk '{print $1}')
```
```bash 
oc describe deployments.apps hamster
```

### VPA component:

* Recommender - it monitors the current and past resource consumption and, based on it, provides recommended values for the containers' cpu and memory requests.
* Updater - it checks which of the managed pods have correct resources set and, if not, kills them so that they can be recreated by their controllers with the updated requests.
* Admission Plugin - it sets the correct resource requests on new pods (either just created or recreated by their controller due to Updater's activity).


###  Update Policy

* "Initial": VPA only assigns resources on Pod creation and does not change them during lifetime of the Pod.
* "Auto" (default): VPA assigns resources on Pod creation and additionally can update them during lifetime of the Pod, including evicting / rescheduling the Pod.
* "Off": VPA never changes Pod resources. The recommender still sets the recommended resources in the VPA object. This can be used for a “dry run”.

More info: [source](https://github.com/kubernetes/autoscaler/tree/master/vertical-pod-autoscaler)

5. Change Policy to off:

* Update hamster.yaml adding updatePlicy to hamster-vpa:
```diff
diff --git a/vertical-pod-autoscaler/examples/hamster.yaml b/vertical-pod-autoscaler/examples/hamster.yaml
index a4928605a..9fd5e77d5 100644
--- a/vertical-pod-autoscaler/examples/hamster.yaml
+++ b/vertical-pod-autoscaler/examples/hamster.yaml
@@ -19,6 +19,8 @@ spec:
     apiVersion: "apps/v1"
     kind: Deployment
     name: hamster
+  updatePolicy:
+    updateMode: "Off"
   resourcePolicy:
     containerPolicies:
       - containerName: '*'
```
```bash
oc get vpa hamster-vpa
```


6. Restart pods:
```bash
oc rollout restart deployment hamster
```

7. Check resource for new pods and deployment:
```bash
oc describe pod $(kubectl get pod -l app=hamster | grep Running | head -n 1 | awk '{print $1}')
```
```bash
oc describe deployments.apps hamster
```
8. Clean
```bash
oc delete -f examples/hamster.yaml
```
change project to the previous
```bash
oc project $(oc whoami )-project
```
```bash
oc delete project test-vpa
```


[go to home](../../../README.md)

[go to next](../exercise42/README.md)
