# Exercise 12:  Get information about horizontalpodautoscalers

## Print  all resources and api versions
1. Print all api resources
```bash
oc api-resources
```
* Print all api resources isolated in namespaces:
```bash
oc api-resources --namespaced=true
```

* Print all global api resources
```bash
oc api-resources --namespaced=false
```

2. Print all api versions
```
oc api-versions
```

## Compare changes between version on horizontalpodautoscaler resource
1. Print resource
```bash
oc explain horizontalpodautoscalers
```
2. Print options for spec for version v1
```bash
oc explain hpa.spec --api-version=autoscaling/v1
```

3. Print options for spec for version v2beta1
```bash
oc explain hpa.spec --api-version=autoscaling/v2beta1
```

4. Which api version needs to be used to set hpa over memory metrics usage ?

[go to home](../../../README.md)

[go to next](../exercise13/README.md)
