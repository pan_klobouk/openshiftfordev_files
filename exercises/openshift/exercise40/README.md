# Exercise 40: Use an HPA based on Memory

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise40/
```

2. Create new HPA
```bash
oc apply -f hpa-v2-memory.yaml
```
3. Start a load-generator (benchmark)
```bash
oc run -it load-generator \
  --rm \
  --image=busybox \
  --restart=Never \
  -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://php-apache/randtest.php; done"
```

4. Watch Pod changes
```bash
watch -d -n 1 "oc get hpa"
```

5. Clean
```bash
oc delete project hpa-test
```
```bash
oc delete -f php-apache.yaml
```

[go to home](../../../README.md)

[go to next](../exercise41/README.md)
