# Exercise 62: Test emptydir

# Switch back to your CRC cluster 
```bash
oc login -u developer https://api.crc.testing:6443
```

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise62/
```

2. Create a pod with EmptyDir volume
```bash
oc apply -f test-emptydir.yaml
```

3. Connect to the container inside POD
```bash
oc exec -it test-emptydir-demo -- bash
```

4. Write current date in index.html file
```bash
date > /app/index.html
```

5. Exit the container (Ctrl-D) and create port-forward to POD
```bash
oc port-forward test-emptydir-demo 8080:8080
```
```bash
curl 127.0.0.1:8080
```

6. In the second terminal login to minikube and kill container
```bash
ssh -i ~/.crc/machines/crc/id_ecdsa core@192.168.130.11
```
```bash
sudo crictl stop $(sudo crictl ps | grep test-emptydir-demo | awk '{print $1}') \
 && sudo crictl rm $(sudo crictl ps -a | grep test-emptydir-demo | awk '{print $1}')
```
```bash
exit 
```
```bash
oc get all
```

7. Once the container is recreated and when the Pod has status Running, check application
```bash
curl 127.0.0.1:8080
```

8. Clean
```bash
oc delete -f test-emptydir.yaml
```


[go to home](../../../README.md)

[go to next](../exercise63/README.md)
