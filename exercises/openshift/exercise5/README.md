# Exercise 5: Show information about pod restarts

```bash 
oc get pods -o custom-columns="name:.metadata.name,\
   container.name:.status.containerStatuses.*.name,\
   restart:.status.containerStatuses.*.restartCount"
```

[go to home](../../../README.md)

[go to next](../exercise6/README.md)
