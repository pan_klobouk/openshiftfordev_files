# Exercise 20: Overwrite s2i scripts

1. Checking where is s2i files inside image
```bash
docker inspect \
  --format='{{ index .Config.Labels "io.openshift.s2i.scripts-url" }}' \
  nginx-centos7
```

2. Go to source-to-image repo directory:
```
cd ~/source-to-image/examples/nginx-centos7/test/test-app
```

3. Create .s2i/bin/assemble file in source-to-image repo:

```bash
mkdir -p .s2i/bin
cat <<EOF > .s2i/bin/assemble
#!/bin/bash
echo "Before assembling"

/usr/libexec/s2i/assemble
rc=\$?

if [ \$rc -eq 0 ]; then
    echo "After successful assembling"
else
    echo "After failed assembling"
fi

exit \$rc
EOF
```

3. Create new application
```bash
s2i build . nginx-centos7 nginx-centos7-app
```

Output:
```bash
Before assembling
---> Building and installing application from source...
After successful assembling
Build completed successfully
``` 


[go to home](../../../README.md)

[go to next](../exercise21/README.md)
