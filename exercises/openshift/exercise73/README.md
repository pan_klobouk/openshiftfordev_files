# Exercise 73: Create a secret

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise73/
```
2. Create a secret
```bash
oc create -f mysecret.yaml
```

3. Create a pod with mounted secret
```bash
oc create -f busybox-secret.yaml
```

4. Check POD
```bash
oc exec -it busybox-secret -- sh
```

5. Create POD with secret as env variable
```bash
oc create -f busybox-secret-env.yaml
```

6. Check
```bash
oc exec busybox-secret-env -- env
```
```bash
oc exec busybox-secret-env -- env | grep -E "SECRET_USERNAME|SECRET_PASSWORD"
```

7. Clean 
```bash
oc delete -f busybox-secret-env.yaml --grace-period 0 --force
```
```bash
oc delete -f busybox-secret.yaml --grace-period 0 --force
```

[go to home](../../../README.md)

[go to next](../exercise74/README.md)
