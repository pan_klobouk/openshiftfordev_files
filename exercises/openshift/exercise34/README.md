# Exercise 34: Blue/Green strategy

1. Create blue application 
```bash
oc new-app https://github.com/devops-with-openshift/bluegreen#master --name=blue
```

2. Expose service 
```bash 
oc expose service blue --name=bluegreen --hostname=bluegreen.crc-okd.mnieto.pl
```

3. Check application 
```bash 
curl http://$(oc get route/bluegreen -o jsonpath='{.status.ingress[0].host}')/
```

4. Check application in we bbrowser: 
```bash
firefox  http://$(oc get route/bluegreen -o jsonpath='{.status.ingress[0].host}')/
``` 

5. Create green application
```bash 
oc new-app https://github.com/devops-with-openshift/bluegreen#green --name=green
```

6. Change traffic to green application 
```bash 
oc patch route/bluegreen -p '{"spec":{"to":{"name":"green"}}}'
```
```bash
curl http://$(oc get route/bluegreen -o jsonpath='{.status.ingress[0].host}')/
```
```bash 
curl http://$(oc get route/bluegreen -o jsonpath='{.status.ingress[0].host}')/
```


6. Rollback traffic to blue application 
```bash 
oc patch route/bluegreen -p '{"spec":{"to":{"name":"blue"}}}'
```
```bash
curl http://$(oc get route/bluegreen -o jsonpath='{.status.ingress[0].host}')/
```
```bash 
curl http://$(oc get route/bluegreen -o jsonpath='{.status.ingress[0].host}')/
```

7. Clean 
```bash 
oc delete all -l app=blue
```
```bash
oc delete all -l app=green
```


[go to home](../../../README.md)

[go to next](../exercise35/README.md)
