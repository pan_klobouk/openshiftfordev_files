# Exercise 44: Test default limits in project

---
>**Warning**
>
>You need cluster-admin rights !!

---


1. Create new project :
```bash
oc new-project $(oc whoami)-limit-example
```
2. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise44/
```

3. Add limits:
```bash
oc apply -f limits.yaml
```

4. Check limits
```bash
oc describe limits
```

5. Create POD:
```bash
oc apply -f busybox.yaml
```

6. Check Pod
```bash
oc describe pod busybox
```

7. Clean project:
```bash
oc delete project $(oc whoami)-limit-example
```

[go to home](../../../README.md)

[go to next](../exercise45/README.md)
