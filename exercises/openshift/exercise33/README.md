# Exercise 33: Recreate strategy

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise33
```

2. Create the first application
```bash
oc apply -f app-v1/.
```
3. Check all
```bash
oc get all -l app=my-app
```

4. Watch pods
```bash
watch -n 1 -d "oc get pod -l app=my-app "
```
or
```bash
oc get pod -l app=my-app --watch-only
```

5. In the second terminal check an application
```bash
while true; do sleep 0.4;\
 curl  http://$(oc get route/my-app\
 -o jsonpath='{.status.ingress[0].host}')/;\
done
```

5. In the third terminal create the second application
```bash
oc apply -f app-v2/.
```

6. Clean
```bash
oc delete all -l app=my-app
```

[go to home](../../../README.md)

[go to next](../exercise34/README.md)
