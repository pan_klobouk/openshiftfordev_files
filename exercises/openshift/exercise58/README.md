# Exercise 58: Service mesh - deployment blue-green

1. Set two version of  recommendation application:
```bash
oc create -f istiofiles/destination-rule-recommendation-v1-v2.yml
```
```bash
oc create -f istiofiles/virtual-service-recommendation-v2.yml
```
2. Check application:
```bash
export GATEWAY_URL=$(oc get route istio-ingressgateway -n istio-system -o=jsonpath="{.spec.host}")
```
```bash
curl $GATEWAY_URL/customer
```

3. Change to version v1:
```bash
oc replace -f istiofiles/virtual-service-recommendation-v1.yml
```

4. Clean virtual server:
```bash
oc delete -f istiofiles/virtual-service-recommendation-v1.yml
```

[go to home](../../../README.md)

[go to next](../exercise59/README.md)

---
source: https://redhat-scholars.github.io/istio-tutorial/
