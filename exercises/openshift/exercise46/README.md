# Exercise 46: Pod with shared storage between containers

![MultiContainerPods](images/MultiContainerPods.png)

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise46/
```

2. Create an application
```bash
oc apply -f multi-container.yaml
```

3. Create a service and route
```bash
oc expose deployment mc1 --port=8080
```
```bash
oc expose service mc1 --hostname=mc1-$(oc project -q).crc-okd.mnieto.pl
```

4. Expose the service
```bash
while true; do sleep 0.1;\
 curl $(oc get route/mc1 -o jsonpath='{.status.ingress[0].host}');\
done
```

5. Check logs
```bash
oc logs -f mc1-<tab> 1st
```
```bash
oc logs -f mc1-<tab> 2nd
```

6. Check files with data
```bash
oc exec -it mc1-<tab> -c 1st -- cat /opt/bitnami/apache/htdocs/index.html
```
```bash
oc exec -it mc1-<tab> -c 2nd -- cat /html/index.html
```
7. Clean
```bash
oc delete -f multi-container.yaml
```

[go to home](../../../README.md)

[go to next](../exercise47/README.md)
