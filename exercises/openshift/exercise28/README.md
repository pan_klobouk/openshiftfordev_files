# Exercise 28: Create DeamonSet


1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise28/
```

2. Create a POD
```bash
oc apply -f deamonset-nginx.yaml
```

3. Check a POD 
```bash
oc get pods -o wide 
```

print describe: 
```
oc describe pod nginxds-<tab>
```

4. Clean
```bash
oc delete -f deamonset-nginx.yaml
```

[go to home](../../../README.md)

[go to next](../exercise29/README.md)
