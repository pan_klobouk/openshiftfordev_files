# Exercise 55: Route restrictions

1. Settings restrictions to one ip address (random)
```bash
oc annotate route my-application haproxy.router.openshift.io/ip_whitelist=13.12.12.12
```

2. Check configuration
```bash
oc describe route my-application
```

3. Check application
```bash
curl -v http://$(oc get route/my-application -o jsonpath='{.status.ingress[0].host}') -o /dev/null
```

4. Setting our ip address
* set gateway for CRC network: 
```bash
oc annotate route my-application haproxy.router.openshift.io/ip_whitelist=192.168.130.1 --overwrite
```
* or set your public ip address: 
```bash
oc annotate route my-application haproxy.router.openshift.io/ip_whitelist=$(curl ifconfig.me) --overwrite
```
* or remove annotates if your OCP cluster is behind LB
```bash
oc annotate route my-application haproxy.router.openshift.io/ip_whitelist-
```


5. Check application
```bash
curl -v http://$(oc get route/my-application -o jsonpath='{.status.ingress[0].host}') -o /dev/null
```

[go to home](../../../README.md)

[go to next](../exercise56/README.md)
