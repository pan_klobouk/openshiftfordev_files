# Exercise 27: Triggers

1. Check current triggers settings
```bash
oc set triggers dc/my-application
```

2. Change triggers to manual
```bash
oc set triggers dc/my-application --manual
```

3. Start new build
```bash
oc start-build builder --follow
```

4. Check history
```bash
oc rollout history dc/my-application
```

5. Change triggers back to auto
```bash
oc set triggers dc/my-application --auto
```

6. Check history
```bash
oc rollout history dc/my-application
```

[go to home](../../../README.md)

[go to next](../exercise28/README.md)
