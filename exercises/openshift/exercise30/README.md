# Exercise 30: Cronjob with cowsay application

1. Create cronjob with application cowsay using our image:
```bash
oc create cronjob \
  cowsay --image=registry.gitlab.com/greenitnet/cowsay:latest \
  --schedule='*/1 * * * *' \
  --restart=OnFailure  --
```

2. Check pods 
```bash 
oc get pods 
```

3. Check jobs 
```bash 
oc get cronjobs
```
```bash
oc get jobs
```

4. Get logs from pods
```bash 
oc logs <cronjob pod>
```

![cowsay](images/cowsay.png)

5. Clean 
```bash 
oc delete cj cowsay
```

[go to home](../../../README.md)

[go to next](../exercise31/README.md)
