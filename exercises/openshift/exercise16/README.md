# Exercise 16: Create application based on Dockerfile 

1. Create application 
```bash 
oc new-build \
  --name='php-test' \
  -l name='php-test' \
  https://gitlab.com/greenitnet/php-docker-test.git
```

2. Check logs 
```bash 
oc logs -f bc/php-test
```

or start new build with higher log level and follow option 
```bash 
oc start-build --build-loglevel 5 php-test --follow
```

3. Check objects in projects
```bash 
oc get all --show-labels
```

4. Clean 
```bash 
oc delete all -l name=php-test
```

[go to home](../../../README.md)

[go to next](../exercise17/README.md)
