# Exercise 43: Add pod limits as container environment variable

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise43/
```

2. Create a Pod with an environment variable with limits 
```bash
oc create -f env_limits.yaml
```
3. Checking a container environment variable
```bash
oc exec envlimit -- env | grep MEMORY | sort
```

4. Clean
```bash
oc delete -f env_limits.yaml --grace-period=0 --force
```

[go to home](../../../README.md)

[go to next](../exercise44/README.md)
