# Exercise 13: Liveness and readiness probe

## Create application hello world writing to redis:

1. Create Pods
```bash
oc apply -f https://gitlab.com/greenitnet/hello_world_flask/-/raw/master/okd_files/hello_world_flask.yaml
```

2. Print logs
```bash
oc logs -f myapp<tab>
```

3. Print events
```bash
oc get events -w
```
4. In the second terminal print description for pod
```bash
oc describe pod myapp<tab>
```

5. Create Pod with redis
```bash 
oc apply -f https://gitlab.com/greenitnet/hello_world_flask/-/raw/master/okd_files/hello_world_redis.yaml
```

6. Expose service http and check application:
```bash
oc expose service http
```

7. Add CRC ip address and application host to /etc/hosts (as root)
```bash 
echo "192.168.130.11 $(oc get route/http -o jsonpath='{.status.ingress[0].host}')"
```

8. Check applicaion
```bash 
curl http://$(oc get route/http -o jsonpath='{.status.ingress[0].host}')
```

9. Check status of pod my-app:
```bash
oc describe pod myapp-<tab>
```

10. Clean
```bash
oc delete -f https://gitlab.com/greenitnet/hello_world_flask/-/raw/master/okd_files/hello_world_flask.yaml
oc delete -f https://gitlab.com/greenitnet/hello_world_flask/-/raw/master/okd_files/hello_world_redis.yaml
```

[go to home](../../../README.md)

[go to next](../exercise14/README.md)
