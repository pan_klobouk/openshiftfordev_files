# Exercise 61: Service mesh - deployment shadow (Dark Launch)

1. Check pods:
```bash
oc get pods -l app=recommendation
```

2. Check destination rule (DR)
```bash
oc get destinationrule
```

3. Create DR and mirror VS:
```bash
oc create -f istiofiles/destination-rule-recommendation-v1-v2.yml
```
```bash
oc create -f istiofiles/virtual-service-recommendation-v1-mirror-v2.yml
```

4. Check:
```bash
export GATEWAY_URL=$(oc get route istio-ingressgateway -n istio-system -o=jsonpath="{.spec.host}")
while true;\
do \
  curl ${GATEWAY_URL}/customer;\
  sleep 0.2;\
done
```

5. Check logs for v2 version
```bash
oc logs -f $(oc get pods |grep recommendation-v2|awk '{print $1}') \
  -c recommendation
````

6. clean:
```bash
oc delete -f customer/kubernetes/Deployment.yml
```
```bash
oc delete -f customer/kubernetes/Service.yml
```
```bash
oc delete -f customer/kubernetes/Gateway.yml
```
```bash
oc delete -f preference/kubernetes/Deployment.yml
```
```bash
oc delete -f preference/kubernetes/Service.yml
```
```bash
oc delete -f recommendation/kubernetes/Deployment.yml
```
```bash
oc delete -f recommendation/kubernetes/Service.yml
```
```bash
oc apply -f recommendation/kubernetes/Deployment-v2.yml
```

[go to home](../../../README.md)

[go to next](../exercise62/README.md)

---
source: https://redhat-scholars.github.io/istio-tutorial/
