# Exercise 51: Service my-service -> www.nobleprog.pl

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise51/
```

2. Create an ExternalName service
```bash
oc apply -f service-external-name.yaml
```

3. Print all services:
```bash
oc get svc
```

4. Check service
```bash
oc exec dnsutils -- dig my-service A +search
```
output 
```
my-service.test-network.svc.cluster.local. 30 IN CNAME www.nobleprog.pl
www.nobleprog.pl.	60	IN	A	35.158.142.198
```

5. Clean
```bash
oc delete -f service-external-name.yaml
```
```bash
oc delete pod dnsutils
```


[go to home](../../../README.md)

[go to next](../exercise52/README.md)
