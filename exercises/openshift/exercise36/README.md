# Exercise 36: Update and rollback

1. Create Cat of the Day application
```bash
oc new-app \
  --name='cotd' \
  -l app=cotd \
  php:7.3~https://github.com/devops-with-openshift/cotd.git \
  -e SELECTOR=cats \
  --as-deployment-config=true
```

2. Expose application
```bash
oc expose service cotd --name=cotd -l app='cotd' --hostname=cotd.crc-okd.mnieto.pl
```

3. Check all objects for application cotd
```bash
oc get all -l app=cotd
```

4. Check Deployment
```bash
oc describe dc cotd
```
```bash
firefox  http://$(oc get route/cotd -o jsonpath='{.status.ingress[0].host}')
```

5. Change env variable, which trigger deployment
```bash
oc set env dc cotd SELECTOR=cities
```

6. Check application:
```bash
firefox  http://$(oc get route/cotd -o jsonpath='{.status.ingress[0].host}')
```


7. Check rollout history
```bash
oc rollout history dc cotd
```
```bash
oc rollout history dc cotd --revision=1
```
```bash
oc rollout history dc cotd --revision=2
```

8. Rollback to previous version
```bash
oc rollback cotd --to-version=1 
```

 
```bash
oc rollout undo dc cotd --to-revision=1
```

9. Check Deployment
```bash
oc describe dc cotd
```
```bash
oc set triggers dc cotd
```

10. Enable triggers:

> Warning: the following images triggers were disabled: cotd:latest
You can re-enable them with:
```bash
oc set triggers dc cotd --auto
```


11. Change it again: 
```bash
oc set env dc cotd SELECTOR=cities
```

12. Check rollout hostory: 
```bash
oc rollout history dc cotd
```

13. Rollout changes to previous version again: 
```bash
oc rollout undo dc cotd --to-revision=1
```

14. Clean
```bash
oc delete all -l app=cotd
```

[go to home](../../../README.md)

[go to next](../exercise37/README.md)
