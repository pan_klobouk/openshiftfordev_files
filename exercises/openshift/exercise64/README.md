# Exercise 64: LAMP application

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise64/
```
2. Create an application LAMP
```bash
oc apply -f test-lamp-subpath.yaml
```

3. Create route
```bash
oc expose svc wordpress --hostname wordpress.crc-okd.mnieto.pl
```

4. Check wordpress application
```
firefox wordpress.crc-okd.mnieto.pl
```


5. Login to crc and check volume-subpaths
```bash
ssh -i ~/.crc/machines/crc/id_ecdsa core@192.168.130.11
```
```bash
sudo -i
```
```bash
cd /var/lib/kubelet/pods/$(crictl inspectp $(crictl pods | grep wordpress | awk '{print $1}') | jq '.status.metadata.uid' | tr -d '"')
```

6. Exit from crc

7. Clean
```bash
oc delete -f test-lamp-subpath.yaml
```
```bash
oc delete routes wordpress
```

[go to home](../../../README.md)

[go to next](../exercise65/README.md)
