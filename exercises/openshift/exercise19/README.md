# Exercise 19: Build s2i image in OpenShift

## Test application in desktop

1. Clone repository
```bash
git clone https://github.com/openshift-katacoda/simple-http-server.git
cd simple-http-server
```

2. Build s2i image and application
* s2i image: 
```bash
docker build -t simple-http-server .
```

* application image: 
```bash
s2i build https://gitlab.com/greenitnet/php-docker-test.git simple-http-server simple-http --ref s2i --context-dir www
```

3. Run and check application
* run 
```bash
docker run --rm -p 8080:8080 simple-http
```

* Check:
```bash
curl -v 127.0.0.1:8080
```

## Build S2i image in Openshift

1. Build s2i image
```bash
oc new-build \
   --strategy=docker \
   --name simple-http-server \
   --code https://github.com/openshift-katacoda/simple-http-server
```

2. Check objects in our projects:
```bash
oc get all
```

3. Create application using new s2i image
```bash
oc new-app \
   -l name=php-test \
   simple-http-server~https://gitlab.com/greenitnet/php-docker-test.git#s2i \
   --context-dir www
```

4. Check objects:
```bash
oc get all
```

5. Expose route
```bash
oc expose svc/php-docker-test
```

6.  Add CRC ip address and application host to /etc/hosts (as root)
```bash
echo "192.168.130.11 $(oc get route/php-docker-test -o jsonpath='{.status.ingress[0].host}')"
```

7. Check application
```bash
curl http://$(oc get route/php-docker-test -o jsonpath='{.status.ingress[0].host}')
```

8. Clean
```bash
oc delete all -l name=php-test
oc delete all -l build=simple-http-server 
```

[go to home](../../../README.md)

[go to next](../exercise20/README.md)
