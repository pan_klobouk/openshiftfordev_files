# Exercise 57: Service mesh - Create recommendation application

## Switch to cluster OCP

oc login -u _name_ -p _name_nobleprog  https://api.okdnobleprog.gcp.mnieto.pl:6443

## Application

There are three different and super simple microservices
in this system and they are chained together in the following sequence:

customer → preference → recommendation

For now, they have a simple exception handling solution
for dealing with a missing dependent service:
it just returns the error message to the end-user.

## Setup

1. Create new project:
```bash
oc new-project $(oc whoami)-istio
```
2. Enable service mesh for all application in project
```bash
oc annotate namespaces $(oc whoami)-istio   sidecar.istio.io/inject=true
```

3. Your project need to be added in ServiceMeshMemberRoll for Service mesh (you need cluster admin role)
```bash
oc edit ServiceMeshMemberRoll -n istio-system 
```

4. Check
```bash
oc describe namespace $(oc whoami)-istio
```

## Create application

1. Clone application git repo:
```bash
git clone https://github.com/redhat-scholars/istio-tutorial.git -c ~/itio-tutorial
cd ~/istio-tutorial
```

2. Create applications:
```bash
oc apply -f customer/kubernetes/Deployment.yml
```
```bash
oc create -f customer/kubernetes/Service.yml
```
```bash
oc create -f customer/kubernetes/Gateway.yml
```
```bash
oc apply -f preference/kubernetes/Deployment.yml
```
```bash
oc create -f preference/kubernetes/Service.yml
```
```bash
oc apply -f recommendation/kubernetes/Deployment.yml
```
```bash
oc create -f recommendation/kubernetes/Service.yml
```

3. Add second version for recommendation app:
```bash
oc apply -f recommendation/kubernetes/Deployment-v2.yml
```

4. In the terminal window check service:
```bash
export GATEWAY_URL=$(oc get route istio-ingressgateway -n istio-system -o=jsonpath="{.spec.host}")
```
```bash
curl $GATEWAY_URL/customer
```

```bash
while true; do curl $GATEWAY_URL/customer; sleep 0.3; done
```

5. Check kiali dashbooard

6. Check grafana


[go to home](../../../README.md)

[go to next](../exercise58/README.md)

---
source: https://redhat-scholars.github.io/istio-tutorial/
