# Exercise 25: Change referencePolicy in ImageStream 

1. Check current BuildConfig
build: 
```bash 
oc get bc/builder -o yaml
```

image stream: 
```bash
oc get is/s2i-go -o yaml
```

2. Change referencePolicy to "Local"
```bash 
oc patch is/s2i-go -p '{"spec":{"tags":[{"name":"latest","referencePolicy":{"type":"Local"}}]}}'
```

3. Check BuildConf
```bash 
oc get bc/builder -o yaml
```

[go to home](../../../README.md)

[go to next](../exercise26/README.md)
