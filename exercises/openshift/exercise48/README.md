# Exercise 48: Application with init container


1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise48/
```

2. Create an application
```bash
oc apply -f init-containers.yaml
```

3. Print all objects
```bash
oc get all
```

4. Create service and route
```bash
oc expose pod init-demo --port=8080
```
```bash
oc expose svc init-demo  --hostname=init-$(oc project -q).crc-okd.mnieto.pl
```

5. Check application
```bash
curl $(oc get route/init-demo -o jsonpath='{.status.ingress[0].host}')
```
```bash
firefox $(oc get route/init-demo -o jsonpath='{.status.ingress[0].host}')
```

6. Check logs: 
```bash 
oc logs init-demo
```
```bash
oc logs init-demo install
```
```bash
oc logs init-demo apache
```

7. Clean
```bash
oc delete -f init-containers.yaml
```
```bash
oc delete svc,route init-demo
```

[go to home](../../../README.md)

[go to next](../exercise49/README.md)
