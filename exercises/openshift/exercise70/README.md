# Exercise 70: Start pod with set UID

---
> **Warning**
>
> You need cluster-admin rights !!
---


1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise70/
```

2. Create a pod with security context
```bash
oc create -f busybox-uid.yaml
```
3. Check
```bash
oc exec -it busybox-uid -- id
```
4. Create a pod with readonly filesystem
```bash
oc create -f busybox-readonly.yaml
```

5. Check
```bash
oc exec -it busybox-readonly -- touch x
```

6. Clean
```bash
oc delete -f busybox-readonly.yaml --grace-period 0 --force
```
```bash
oc delete -f busybox-uid.yaml  --grace-period 0 --force
```


[go to home](../../../README.md)

[go to next](../exercise71/README.md)
