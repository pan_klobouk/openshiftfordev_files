# Exercise 39: Use an HPA based on CPU


1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise39/
```

2. Create application
```bash
oc apply -f php-apache.yaml
```

3. Set a Pod autoscale based on CPU
```bash
oc autoscale deployment php-apache --cpu-percent=50 --min=1 --max=10
```
or
```bash
oc apply -f hpa-v1.yaml
```

4. Start a load-generator (benchmark)
```bash
oc run -i --tty load-generator \
  --rm \
  --image=busybox \
  --restart=Never \
  -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://php-apache/randtest.php; done"
```
5. Watch Pod changes
```bash
watch -d -n 1 "oc get hpa"
```

6. Check pod stats:
```bash 
oc adm top pods
```

7. How it looks in v2 version configuration format?
```bash
oc get hpa.v2beta2.autoscaling php-apache -o yaml
```
```bash
oc get hpa.v2.autoscaling php-apache -o yaml
```
```bash
oc get hpa.v1.autoscaling php-apache -o yaml
```

8. Clean
```bash
oc delete hpa php-apache
```

[go to home](../../../README.md)

[go to next](../exercise40/README.md)
