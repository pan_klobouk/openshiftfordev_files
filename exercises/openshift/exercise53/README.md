# Exercise 53: Create route with ssl for my-application

1. Create route
* version for CRC:
```bash
oc create route edge my-application-ssl \
  --service=my-application \
  --key='/home/nobleprog/workshop/exercises/openshift/exercise23/ssl/key.pem' \
  --cert='/home/nobleprog/workshop/exercises/openshift/exercise23/ssl/cert.pem' \
  --hostname=my-application-ssl-$(oc project -q).crc-okd.mnieto.pl
```
* version for OKD
```bash
oc create route edge my-application-ssl \
  --service=my-application \
  --hostname=my-application-ssl-$(oc project -q).apps.okdnobleprog.gcp.mnieto.pl
```

2. Check application
```bash
curl https://$(oc get route/my-application-ssl -o jsonpath='{.status.ingress[0].host}')
```
```bash
curl http://$(oc get route/my-application-ssl -o jsonpath='{.status.ingress[0].host}')
```

3. Print describe for my-application-ssl route
```bash
oc describe route my-application-ssl
```

4. Check how to enable http access
```bash
oc explain route.spec.tls
```

5. Turn on http access by edit route object and add insecureEdgeTerminationPolicy with value *Allow*
```bash
oc edit route/my-application-ssl
```
```diff
...
spec:
  host: my-application-ssl-test.apps.okdnobleprog.gcp.mnieto.pl
  port:
    targetPort: 8080-tcp
  tls:
    certificate: |
    key: |
+    insecureEdgeTerminationPolicy: Allow
    termination: edge
  to:
    kind: Service
    name: my-application
    weight: 100
  wildcardPolicy: None
```

6. Check application again
```bash
curl https://$(oc get route/my-application-ssl -o jsonpath='{.status.ingress[0].host}')
```
```bash
curl http://$(oc get route/my-application-ssl -o jsonpath='{.status.ingress[0].host}')
```

7. Change insecureEdgeTerminationPolicy value to Redirect
```bash
oc patch route my-application-ssl -p '{"spec":{"tls":{"insecureEdgeTerminationPolicy": "Redirect"}}}'
```

8. Check application again
```bash
curl https://$(oc get route/my-application-ssl -o jsonpath='{.status.ingress[0].host}')
```
```bash
curl -vL http://$(oc get route/my-application-ssl -o jsonpath='{.status.ingress[0].host}')
```

[go to home](../../../README.md)

[go to next](../exercise54/README.md)
