# Exercise 75: Application logs processing


1. Create application
```bash
oc new-app registry.gitlab.com/greenitnet/php-docker-test:latest \
  -l app=test-apache-json \
  -e multiplier=100 \
  --name=test-apache-json
```

2. Expose and check application
```bash
oc expose svc test-apache-json --port=8080
host=$(oc get route/test-apache-json -o jsonpath='{.status.ingress[0].host}')
curl http://${host}/randtest.php
```

3. Check logs in terminal
```bash
oc logs -f test-apache-json-<tab>
```

4. If we have turned on logs on CRC go to kibana and check application logs there 
   or visit kibana (in case cluster OCP) https://kibana-openshift-logging.apps.okdnobleprog.gcp.mnieto.pl/

5. Clean
```bash
oc delete all -l app=test-apache-json
```

[go to home](../../../README.md)

[go to next](../exercise76/README.md)
