# Exercise 71: Turn off capabilities

---
> **Warning**
>
> You need cluster-admin rights !!
---

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise71/
```

2. Create a pod
```bash
oc create -f myapp.yaml
```

3. Check capabilities
```bash
oc exec myapp -- grep Cap /proc/1/status
```

4. Create a pod with capabilities
```bash
oc create -f myapp-cap.yaml
```

5. Check the pod
```bash
oc exec myapp-cap -- grep Cap /proc/1/status
```

6. Clean
```bash
oc delete -f myapp.yaml --grace-period 0 --force
```
```bash
oc delete -f myapp-cap.yaml --grace-period 0 --force 
```


[go to home](../../../README.md)

[go to next](../exercise72/README.md)
