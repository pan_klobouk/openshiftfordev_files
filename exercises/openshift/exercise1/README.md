# Exercise 1: Run CodeReadyContainers

1. Start minikube
```bash
crc setup
crc config view
crc config set cpus 6
crc config set memory 16384
crc config set disk-size 50
crc config set enable-cluster-monitoring true
crc config set pull-secret-file /home/nobleprog/crc/pull-secret.json
crc config view 
crc start 
```

```bash
Started the OpenShift cluster.

The server is accessible via web console at:
  https://console-openshift-console.apps-crc.testing

Log in as administrator:
  Username: kubeadmin
  Password: 6YFGa-uGZj4-KQfuP-LnIZw

Log in as user:
  Username: developer
  Password: developer

Use the 'oc' command line interface:
  $ eval $(crc oc-env)
  $ oc login -u developer https://api.crc.testing:6443
```

2. Checking installation
```bash
oc get nodes

```
or using crc 
```bash
eval $(crc oc-env)
oc get nodes 
```

4. Check CRC server
```bash 
ssh -i ~/.crc/machines/crc/id_ecdsa core@192.168.130.11
sudo crictl ps
```


[go to home](../../../README.md)

[go to next](../exercise2/README.md)
