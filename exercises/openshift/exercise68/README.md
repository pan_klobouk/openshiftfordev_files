# Exercise 68: Create a new user


## Login to CRC cluster as kubeadmin
```bash
oc login -u kubeadmin -p$(cat ~/.crc/machines/crc/kubeadmin-password) https://api.crc.testing:6443
```


1. Create a cert directory
```bash
mkdir -p ~/users
```

# Set up htpasswd Authentication

2. Create a htpasswd file
```bash
sudo apt-get -y install apache2-utils
``` 
```bash 
htpasswd -B -c -b ~/users/htpasswd admin $(cat ~/.crc/machines/crc/kubeadmin-password)
```
```bash
htpasswd -b ~/users/htpasswd devuser devpwd
```
```bash
htpasswd -b ~/users/htpasswd test-user test-user
```

3. Create secret with htpasswd file 
```bash 
oc create -n openshift-config secret generic htpasswd-secret --from-file=htpasswd=/home/nobleprog/users/htpasswd
```

4. Update oauth CR 
```bash 
oc get oauth cluster 
```
and add new identityProviders:
```bash 
apiVersion: config.openshift.io/v1
kind: OAuth
metadata:
  name: cluster
spec:
  identityProviders:
  - name: my_htpasswd_provider 
    mappingMethod: claim 
    type: HTPasswd
    htpasswd:
      fileData:
        name: htpasswd-secret
```

# Update htpasswd Authentication

2. Dump current htpasswd file from secret:
```bash 
oc get secret htpasswd-secret -ojsonpath={.data.htpasswd} -n openshift-config | base64 --decode > ~/users/users.htpasswd
```

3. Add or remove users from the users.htpasswd file.
```bash 
htpasswd -bB users.htpasswd <username> <password>
```
or remove 
```
$ htpasswd -D users.htpasswd <username>
```

4. Replace the htpass-secret Secret object with the updated users in the users.htpasswd file:
```bash 
oc create secret generic htpasswd-secret --from-file=htpasswd=users.htpasswd --dry-run=client -o yaml -n openshift-config | oc replace -f -
```

5. If you removed one or more users, you must additionally remove existing resources for each user.
```bash 
$ oc delete user <username>
```
and also from Identity object
```bash 
$ oc delete identity my_htpasswd_provider:<username>
```

# Configuring identity providers using the web console

Administration >  Cluster Settings >  Configuration > OAuth

![OAuth](images/okd_user_oauth_webui.png)



[go to home](../../../README.md)

[go to next](../exercise69/README.md)
