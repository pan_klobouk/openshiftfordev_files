# Exercise 22: S2i incremental build java example

1. Build java application without incremental build
```bash
time s2i build \
  https://github.com/openshift/openshift-jee-sample quay.io/wildfly/wildfly-centos7:latest \
  wildflytest \
  --loglevel=1
```

output:
```bash
...
[INFO] Packaging webapp
[INFO] Assembling webapp [SampleApp] in [/tmp/src/target/SampleApp]
[INFO] Processing war project
[INFO] Copying webapp resources [/tmp/src/src/main/webapp]
[INFO] Webapp assembled in [13 msecs]
[INFO] Building war: /tmp/src/target/ROOT.war
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 12.814 s
[INFO] Finished at: 2021-10-17T19:46:42Z
[INFO] Final Memory: 15M/481M
[INFO] ------------------------------------------------------------------------
...
```

2. Build the same application but with turn on incremental build: 
```bash
time s2i build \
  https://github.com/openshift/openshift-jee-sample quay.io/wildfly/wildfly-centos7:latest \
  wildflytest \
  --incremental=true \
  --loglevel=1
```
output:
```bash
...
[INFO] Packaging webapp
[INFO] Assembling webapp [SampleApp] in [/tmp/src/target/SampleApp]
[INFO] Processing war project
[INFO] Copying webapp resources [/tmp/src/src/main/webapp]
[INFO] Webapp assembled in [13 msecs]
[INFO] Building war: /tmp/src/target/ROOT.war
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 0.840 s
[INFO] Finished at: 2021-10-17T19:47:36Z
[INFO] Final Memory: 13M/481M
[INFO] ------------------------------------------------------------------------
...
```


[go to home](../../../README.md)

[go to next](../exercise23/README.md)
