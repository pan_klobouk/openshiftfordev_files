# Exercise 47: Use revers proxy inside a POD

![multicontainerwebapp](images/multicontainerwebapp.png)

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise47/
```

2. Create a configmap
```bash
oc apply -f multi-container-web-configmap.yaml
```

3. Create an application
```bash
oc apply -f multi-container-web.yaml
```

4. Create a service and route
```bash
oc expose pod mc2 --port=8080
```
```bash
oc expose service mc2  --hostname=mc2-$(oc project -q).crc-okd.mnieto.pl
```

5. Check the configuration
```bash
oc describe service mc2

```

6. Check the application
```bash
while true; do sleep 0.1;\
 curl $(oc get route/mc2 -o jsonpath='{.status.ingress[0].host}');\
done
```

7. Check application logs: 
```bash 
oc logs -f mc2 nginx
```
```bash
oc logs -f mc2 webapp
```

8. Clean
```bash
oc delete -f multi-container-web.yaml
```
```bash
oc delete -f multi-container-web-configmap.yaml
```
```bash
oc delete svc,route mc2
```

[go to home](../../../README.md)

[go to next](../exercise48/README.md)
