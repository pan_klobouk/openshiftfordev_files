# Exercise 66: Tekton


## Set up tekton on your cluster (as kubeadmin) 

1. Go to:  Operators → OperatorHub
2. Find *Red Hat OpenShift Pipelines*
3. Review the information about the Operator and click Install.
4. Click Install
5. Verification:
   * run:
   ```bash
   oc get pods -n openshift-operators
   ```

more info: https://docs.openshift.com/container-platform/4.11/cicd/pipelines/installing-pipelines.html

## Task

1. Clone repo with examples:
  ```bash
  git clone https://github.com/redhat-scholars/tekton-tutorial ~/tekton-tutorial
  cd ~/tekton-tutorial
  ```
2. Create task which will clone repo and print commit, url and directory list:
  ```bash
  cd task
  ```
* Check file *source-lister.yaml*:
  ```bash
  cat source-lister.yaml
  ```
* Create task:
  * Remove namespace from source-lister.yaml
  ```diff
   kind: Task
   metadata:
     name: source-lister
  -  namespace: tektontutorial
   spec:
     params:
       - default: 'https://github.com/redhat-scholars/tekton-tutorial-greeter
  ```

  * Apply it:
  ```bash
  oc apply -f source-lister.yaml
  ```

3. Check task
  ```bash
  tkn task ls
  ```

  * check describe command:
  ```bash
  tkn task describe source-lister
  ```

4. Run task
* Create taskrun yaml:
  ```bash
  cat << EOF > taskrun_source_lister.yaml
  apiVersion: tekton.dev/v1beta1
  kind: TaskRun
  metadata:
    name: source-lister-run
  spec:
    taskRef:
      name: source-lister
    workspaces:
      - name: source # this workspace name must be declared in the Task
        emptyDir: {}
  EOF
  ```
* Start task:
  ```bash
  oc apply -f taskrun_source_lister.yaml
  ```

* Check logs: 
  ```bash
  tkn taskrun logs source-lister-run
  ```

## Clustertask

1. List all taks:
  ```bash
  tkn clustertask ls
  ```

2. Add echoer clustertask: 
  ```bash
  oc apply -f echoer.yaml 
  ```

3. Run task:
  ```bash
  tkn clustertask start echoer --showlog
  ```

### Task summary:

* Tasks are namespace bound i.e. available only in namespace(s) where they were created
* Tasks resources are interacted using tkn task command and its options
* ClusterTasks are available across the cluster i.e. in any namesapce(s) of the cluster
* ClusterTasks resources are interacted using tkn clustertask command and its options

## Pipeline:

1. Create new application using pipeline.
Go to: Add -> Import from Git
and use:
* repo: https://github.com/devfile-samples/devfile-sample-go-basic.git
* edit import Strategy and use 'Builder Image'
* use go Builder Image
* as Application use: Create application
* set name: go-hello-app
* choose Deployment
* Add pipeline and use s2i-go-deployment

Press Create

![Add1](images/go_pipeline_1.png)
![Add2](images/go_pipeline_2.png)

2. Check pipeline:

![Pipeline](images/pipeline_dashboard.png)
![PipelineRun](images/pipelinerun.png)
![PipelineRun2](images/pipelinerun2.png)

3. Check you application
```bash
curl http://$(oc get route/go-hello-app -o jsonpath='{.status.ingress[0].host}')
```

4. Clean: 
  ```bash
  oc delete all -l app.kubernetes.io/instance=go-hello-app
  ```

[go to home](../../../README.md)

[go to next](../exercise67/README.md)
