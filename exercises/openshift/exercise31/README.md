# Exercise 31: Create a POD with a configmap as volume

1. Create namespace test-configmap
```bash
oc new-project $(oc whoami)-test-configmap
```

2. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise31/
```

3. Create a conifg map
```bash
oc apply -f my-app-nginx-configmap.yaml
```

4. Check
```bash
oc get configmap my-app-nginx-conf -o yaml
```

5. Create a deployment
```bash
oc apply -f my-app-nginx.yaml
```

6. Check
```bash
oc describe pod my-application-<tab>
```

7. Create service and route
```bash
oc expose deployment my-application --port=8080
```
```bash
oc expose svc/my-application --hostname=my-application-$(oc project -q).crc-okd.mnieto.pl
```

8. In the second window check site
```bash
curl -v "http://$(oc get route/my-application -o jsonpath='{.status.ingress[0].host}')/"
```

9. Clean
```bash
oc delete project $(oc whoami)-test-configmap
```

[go to home](../../../README.md)

[go to next](../exercise32/README.md)
