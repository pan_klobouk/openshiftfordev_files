# Exercise 45: Test quota in project

---
>**Warning**
>
>You need cluster-admin rights !!

---

1. Create a project quota-example:
```bash
oc new-project $(oc whoami)-quota-example
````

2. Change the directory:
```
cd ~/workshop/exercises/openshift/exercise45/
```

3. Create a quota in namespace:
```bash
oc apply -f quota.yaml
```
```bash
oc describe quota
```

4. Create a pod:
```bash
oc create -f busybox.yaml
```

5. Create limits:
```bash
oc apply -f limits.yaml
```
```bash
oc describe limits
```

6. Create one more time pod busybox:
```bash
oc create -f busybox.yaml
```

7. Check a quota
```bash
oc describe quota
```

8. Create a deployment with application nginx with 5 replicas
* In the first terminal print events stream:
```bash
oc get events --watch
```

* In the second terminal create application:
```bash
oc create deployment nginx-test --image=bitnami/nginx:1.21 --replicas=5
```

9. Clean env by deleting whole namespace:
```bash
oc delete project $(oc whoami)-quota-example
```

[go to home](../../../README.md)

[go to next](../exercise46/README.md)
