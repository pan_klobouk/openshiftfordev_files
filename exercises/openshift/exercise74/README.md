# Exercise 74: Network policy - restricted access from pod

1. Go to directory:
```bash
cd ~/workshop/exercises/openshift/exercise74/
```
2. Create deployment with nginx image
```bash
oc new-app --name=nginx --image=bitnami/nginx -l app=nginx
```

3. Checking connection from different pod:
```bash
oc run -it load-generator \
  --rm \
  --image=busybox \
  --restart=Never \
  -- /bin/sh -c "while sleep 0.01; do wget --spider http://nginx:8080; done"
```

4. In second terminal, add network policy rule:
```bash
oc apply -f nginx-policy.yaml
```

5. Correct label for load-generator to all connection to nginx app:
```bash
oc get pods --show-labels
```
```bash
oc label pod load-generator "access=true"
```

6. Clean
```bash 
oc delete all -l app=nginx
```



[go to home](../../../README.md)

[go to next](../exercise75/README.md)
