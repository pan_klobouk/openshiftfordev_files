# Exercise 42: Knative - hello-world

## Install OpenShift Serverless Operator
1. Go to:  Operators → OperatorHub
2. Find ```Red Hat OpenShift Serverless```
![Serverless](images/openshift_serverless.png)
3. Review the information about the Operator and click Install.
4. On the Install Operator page, check:
   * The Installation Mode is All namespaces on the cluster (default).
   * The Installed Namespace is openshift-serverless.
   * Select the stable channel as the Update Channel
   * Select Automatic or Manual approval strategy.
![serverless_olm](images/openshift_serverless_olm.png)
5. Click Install
6. Verification:
   * run:
   ```bash
   oc get pods -n openshift-serverless
   ```

## Install Knative Serving
1. Go to:  `Operators` → `Installed Operators`
2. Click Knative Serving in the list of Provided APIs for the OpenShift Serverless Operator to go to the Knative Serving tab.
3. Click Create Knative Serving
4. Click Create
![serverless_panel](images/openshift_serverless_panel.png)


## Change default domain for Knative:

* Update config map config-domain.yaml in project knative-serving
```bash
kubectl edit configmap config-domain -n knative-serving
```
and replace ```crc-okd.mnieto.pl``` with ```apps-crc.testing```:
```diff
@@ -37,7 +37,7 @@
     svc.cluster.local: |
       selector:
         app: secret
+  crc-okd.mnieto.pl: ""
-  apps-crc.testing: ""
 kind: ConfigMap
 metadata:
   annotations:
```

## Use Knative Serving
1. Copy code repository:
```bash
git clone https://github.com/knative/docs.git ~/knative-docs
cd ~/knative-docs/code-samples/serving/hello-world/helloworld-python
```

2. Create project knative-helloworld-python in gitlab.com

3. Build the container on your local machine

```bash
docker build -t {your_gitlab_account}/knative-helloworld-python .
```

*  Push the container to docker registry
```bash
docker push {your_gitlab_account}/knative-helloworld-python
```

4. Deploy application:

* Create service.yaml manifest with your gitlab project

```bash
apiVersion: serving.knative.dev/v1
kind: Service
metadata:
  name: helloworld-python
spec:
  template:
    spec:
      containers:
      - image: {your_gitlab_account}/knative-helloworld-python
        env:
        - name: TARGET
          value: "Python Sample v1"
```
You can use ready image: registry.gitlab.com/greenitnet/images/knative-helloworld-python:latest

or edit existing file service.yaml:
```diff
diff --git a/code-samples/serving/hello-world/helloworld-python/service.yaml b/code-samples/serving/hello-world/helloworld-python/service.yaml
index 8713e260..31b74fa2 100644
--- a/code-samples/serving/hello-world/helloworld-python/service.yaml
+++ b/code-samples/serving/hello-world/helloworld-python/service.yaml
@@ -2,12 +2,11 @@ apiVersion: serving.knative.dev/v1
 kind: Service
 metadata:
   name: helloworld-python
-  namespace: default
 spec:
   template:
     spec:
       containers:
-      - image: docker.io/{username}/helloworld-python
+      - image: registry.gitlab.com/greenitnet/images/knative-helloworld-python:latest
         env:
         - name: TARGET
           value: "Python Sample v1"

```

* deploy service using oc:
```bash
oc apply --filename service.yaml
```
or using kn took:
```bash
kn service create helloworld-python --image=registry.gitlab.com/{your_project}/knative-helloworld-python --env TARGET="Python Sample v1"
```
This will wait until your service is deployed and ready,
and ultimately it will print the URL through which you can access the service.

5. Check route to your serverless application:

* using oc/kubectl:
```bash
oc get ksvc helloworld-python  --output=custom-columns=NAME:.metadata.name,URL:.status.url
```
or
```bash
kubectl get ksvc helloworld-python  --output=custom-columns=NAME:.metadata.name,URL:.status.url
```
* using kn:
```bash
kn service describe helloworld-python -o url
```

6. Check number of pods:

```bash
oc get pods
```

7. Verification

```bash
curl -vk $(kn service describe helloworld-python -o url)
```
or
```bash
curl -vk $(oc get ksvc helloworld-python -o jsonpath='{.status.url}')
```

8. Check number of pods
```bash
oc get pods
```

9. Clean
```bash
oc delete --filename service.yaml
```
or
```bash
kn service delete helloworld-python
```


[go to home](../../../README.md)

[go to next](../exercise43/README.md)
