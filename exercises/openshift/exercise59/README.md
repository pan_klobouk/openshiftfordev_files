# Exercise 59: Service mesh - deployment canary

1. Check running pods:
```bash
oc get pods -l app=recommendation
```

2. Create VS with two version:
```bash
oc create -f istiofiles/virtual-service-recommendation-v1_and_v2.yml
```

3. Check:
```bash
export GATEWAY_URL=$(oc get route istio-ingressgateway -n istio-system -o=jsonpath="{.spec.host}")
```
```bash
while true ;\
do \
  curl ${GATEWAY}/customer;\
  sleep 0.2;\
done
```

4. Change weight for service:
```bash
oc edit VirtualService/recommendation
```

5. Clean
```bash
oc delete VirtualService/recommendation
```


[go to home](../../../README.md)

[go to next](../exercise60/README.md)

---
source: https://redhat-scholars.github.io/istio-tutorial/
