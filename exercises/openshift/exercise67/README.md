# Exercise 67: Application with affinity and anti-affinity settings

---
>***Notice***
>
>You need more than one nodes in cluster
> 
> login to OCP cluster
>
```bash
oc login -u your_user  https://api.okdnobleprog.gcp.mnieto.pl:6443
```
---

# Required vs preferred node affinity

1. Check nodes in your cluster
```bash
oc get nodes --show-labels
```

2. Add extra label for one node (on OCP is already added):
```bash
oc label nodes <your-node-name> nobleprog=chooseme
```

3. Create pod with required nodeAffinity:
```bash
cat << EOF > ~/redis_required_nodeaffinity.yaml
apiVersion: v1
kind: Pod
metadata:
  name: redis-required
spec:
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: nobleprog
            operator: In
            values:
            - chooseme
  containers:
  - name: redis
    image: redis
    imagePullPolicy: IfNotPresent
EOF
```

4. Create pod from yaml file and check where pod has been scheduled
```bash
oc apply -f ~/redis_required_nodeaffinity.yaml
```
```bash
oc get pods -o wide
```

---
> rule:
>
> The node **must** have a label with the key ***nobleprog***
> and the value of that label **must** be ***chooseme***
---

5. Create pod with preferred nodeAffinity (label nobleprog: preferred is missing):
```bash
cat << EOF > ~/redis_preferred_nodeaffinity.yaml
apiVersion: v1
kind: Pod
metadata:
  name: redis-preferred
spec:
  affinity:
    nodeAffinity:
      preferredDuringSchedulingIgnoredDuringExecution:
      - weight: 1
        preference:
          matchExpressions:
          - key: nobleprog
            operator: In
            values:
            - preferred
  containers:
  - name: redis
    image: redis
    imagePullPolicy: IfNotPresent
EOF
```


6. Create pod from yaml file and check where pod has been scheduled
```bash
oc apply -f ~/redis_preferred_nodeaffinity.yaml
```
```bash
oc get pods -o wide
```
---
>rule:
>
> The node **preferably** has a label with the key
> ***nobleprog*** and the value ***preferred***.
>
> Weight can be from 1 up to 100 for each instance
> of the ***preferredDuringSchedulingIgnoredDuringExecution***
> affinity type
---

# Inter-pod affinity and anti-affinity

Inter-pod affinity and anti-affinity allow you to constrain
which nodes your Pods can be scheduled on based on the labels
of Pods already running on that node, instead of the node labels.

1. Create redis application with podAntiAffinity (spread pod between nodes):
```bash
cat << EOF > ~/redis_cache_podantiaffinity.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: redis-cache
spec:
  selector:
    matchLabels:
      app: store
  replicas: 3
  template:
    metadata:
      labels:
        app: store
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: app
                operator: In
                values:
                - store
            topologyKey: "kubernetes.io/hostname"
      containers:
      - name: redis-server
        image: redis:3.2-alpine
EOF
```

2. Create deployment and check pods:
```bash
oc apply -f ~/redis_cache_podantiaffinity.yaml
```
```bash
oc get pods -o wide
```

3. Create webserver application on node where redis-cached pod has been deployed:
```bash
cat << EOF > ~/web_podantiaffinity.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: web-server
spec:
  selector:
    matchLabels:
      app: web-store
  replicas: 3
  template:
    metadata:
      labels:
        app: web-store
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: app
                operator: In
                values:
                - web-store
            topologyKey: "kubernetes.io/hostname"
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: app
                operator: In
                values:
                - store
            topologyKey: "kubernetes.io/hostname"
      containers:
      - name: web-app
        image: bitnami/nginx:latest
EOF
```

4. Create deployment and check pods:
```bash
oc apply -f ~/web_podantiaffinity.yaml
```
```bash
oc get pods -o wide
```

5. Clean
```bash
oc delete -f ~/redis_cache_podantiaffinity.yaml
```
```bash
oc delete -f ~/web_podantiaffinity.yaml
```
```bash
oc delete -f ~/redis_preferred_nodeaffinity.yaml
```
```bash
oc delete -f ~/redis_required_nodeaffinity.yaml
```

[go to home](../../../README.md)

[go to next](../exercise68/README.md)
