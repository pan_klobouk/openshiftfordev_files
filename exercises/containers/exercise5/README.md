# Exercise 5: Run application in container using k8s + CRI-O

We will set up minikube cluster with CRI-O runtime.

1. Start minikube
```bash
minikube start \
  --cpus 6 \
  --memory 6g \
  --driver=podman \
  --container-runtime=cri-o
```

## kubectl
  The Kubernetes command-line tool, kubectl, allows you to run
  commands against Kubernetes clusters. You can use kubectl to
  deploy applications, inspect and manage cluster resources,
  and view logs.

show kubectl options:
```bash
kubectl -h
```

## Create application on top of k8s

1. Create pod with nginx
```bash
kubectl run pod-nginx --image nginx
```
2. Check pod
```bash
kubectl get pods
```

3. Connect to pods
```bash
kubectl port-forward pod/pod-nginx 8080:80
```
in second terminal:
```ignorelang
curl -v 127.0.0.1:8080
```
in third terminal check logs
```bash
kubectl logs pod-nginx
```
with option follow:
```bash
kubectl logs -f pod-nginx
```

## Debug application in pod in k8s

* use exec command
```bash
kubectl exec -it pod-nginx -- bash
```
then:
* find where is document root in this container (check /etc/nginx/conf.d/default.conf and look for `root` statement)
```bash
cat /etc/nginx/conf.d/default.conf
```
* check what is inside document root directory
```bash
ls -l /usr/share/nginx/html
```
* overwrite index.html
```
echo "Hello from workshop" >/usr/share/nginx/html/index.html
```
* exit container (CTRL-D) and check application response:
```bash
curl  127.0.0.1:8080
```

## Check application using crictl

* login to minikube
```bash
minikube ssh
sudo -i
```

* Pring all containers/pods
```bash
runc list
crictl ps
crictl pods
```
* show logs from pod-nginx
```bash
crictl logs CONTAINER-ID
```
* show logs from file
```bash
 tail -f /var/log/pods/default_pod-nginx_CONTAINER-ID/pod-nginx/0.log
```
or
```bash
tail -f /var/log/containers/pod-nginx_default_pod-nginx-CONTAINER-ID.log
```


[go to home](../../../README.md)

[go to next](../exercise6/README.md)
