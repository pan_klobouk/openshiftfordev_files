# Exercise 3: Container logs

## Create container with writing to stdout current date
* create sandbox:
```bash
cat << EOF > ~/crio/sandbox_logs.yml
metadata:
  name: sandbox_logs
  namespace: default
dns_config:
  servers:
    - 8.8.8.8
EOF
```
* create pod
```bash
PODLOGS_ID=$(crictl runp ~/crio/sandbox_logs.yml)
```


* Create container definition in YAML
```bash
cat << EOF > ~/crio/container_logs.yml
metadata:
  name: container
image:
  image: busybox
command:
- "sh"
- "-c"
- "while true; do \$(echo date); sleep 1; done"
EOF
```

* Pull image
```bash 
crictl pull busybox
```

* Create container:
```bash
CONTAINERLOGS_ID=$(crictl create $PODLOGS_ID ~/crio/container_logs.yml ~/crio/sandbox_logs.yml)
```

* Start container:
```bash
crictl start $CONTAINERLOGS_ID
```

## Checks logs from container
* show all logs
```bash
crictl logs $CONTAINERLOGS_ID
```

* follow logs from application:
```bash
crictl logs -f $CONTAINERLOGS_ID
```

* show only logs from last 1 minute
```bash
date && crictl logs --since 1m $CONTAINERLOGS_ID
```

* show only 10 last lines
```bash
date && crictl logs -tail 10 $CONTAINERLOGS_ID
```

[go to home](../../../README.md)

[go to next](../exercise4/README.md)
