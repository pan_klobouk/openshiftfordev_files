# Exercise 17: Docker inspect


## Task to do:
1. check docker-engine version
```bash
docker version
```
2. Check docker server information
```bash
docker info
```
3. Pull centos image
  * check tags by skopeo or web page at hub.docker.com
```bash
skopeo inspect --override-os=linux docker://docker.io/centos
```
  * Pull image
```bash
docker pull centos:<check your tag on the list>
```

4. Check centos 8 image digest
```bash
docker images --digests
```
5. Run container from centos with tag
```bash
docker run --rm --name="centos" centos:<check your tag on the list> sleep 300
```
6. Check container size
```bash
docker ps -s
```
7. Run container inspection
```bash
docker inspect centos
```
8. Get ip address from container
```bash
docker inspect --format='{{range .NetworkSettings.Networks }}{{ .IPAddress }} {{end}}' centos
```


9. Write output information in etherpad in format:

your name - image:tag:digest:size:ipadress


[go to home](../../../README.md)

[go to next](../exercise18/README.md)
