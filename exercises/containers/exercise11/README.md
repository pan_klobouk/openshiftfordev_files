# Exercise 11: Deployment Configuration using gitlab-ci.yml

1. Add .gitlab-ci.yml file to cowsay repository

```bash
cat << EOF > .gitlab-ci.yml
image: docker:19.03.12
services:
  - docker:19.03.12-dind

before_script:
  - docker login -u \$CI_REGISTRY_USER -p \$CI_REGISTRY_PASSWORD \$CI_REGISTRY

build:
  stage: build
  script:
    - docker build --no-cache  --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA --tag $CI_REGISTRY_IMAGE:latest -f  dockerfiles/Dockerfile context
    - docker push \$CI_REGISTRY_IMAGE:\$CI_COMMIT_SHA
    - docker push \$CI_REGISTRY_IMAGE:latest
EOF
```

or you can copy it from
```bash
cp ~/workshop/exercises/containers/exercise11/.gitlab-ci.yml ~/cowsay
```

2. Check what has changed in your repository:
```bash
git status
```

2. Add modified files to git, commit changes and push them:

```bash
git add .gitlab-ci.yml
git add <other files> if needed
git commit -m 'comment'
git push
```

3. Check pipeline in gitlab.com

![Gitlab-pipeline](images/gitlab-pipeline.png)

4. Check registry with images

![Gitlab-registry](images/gitlab-registry.png)

5. Run container using your public image
```
docker run --rm registry.gitlab.com/<your account>/cowsay:latest "You are awesome"
```


[go to home](../../../README.md)

[go to next](../exercise12/README.md)
