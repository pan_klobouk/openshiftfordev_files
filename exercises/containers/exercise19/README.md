# Exercise 19: Container security - Fixing cowsay application

* Set you project name in bash variable
````bash
PROJECT=_your_project_from_gitlab_
````

* Change directory to cowsay 
```bash 
cd ~/cowsay
```

* Check our image cowsay (built in previous exercise) 
```bash
trivy image registry.gitlab.com/${PROJECT}/cowsay:v2.0.0
```

* Check only High and Critical severity: 
```bash
trivy image --severity HIGH,CRITICAL  registry.gitlab.com/${PROJECT}/cowsay:v2.0.0
```

* Check trivy output for option --ignore-unfixed:
```bash
trivy image --severity HIGH,CRITICAL --ignore-unfixed registry.gitlab.com/${PROJECT}/cowsay:v2.0.0
```

* Check your images 
```bash 
docker images | grep debian
```

* Pull latest debian version  
```bash 
docker pull debian:bullseye-slim
```

* Update FROM in Dockerfile to latest version for debian:  
Change: `FROM debian:bullseye-20211011-slim` \
To    : `FROM debian:bullseye-slim`  

* Rebuild image: 
```bash
docker build \
  --no-cache=true \
  --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') \
  -t registry.gitlab.com/${PROJECT}/cowsay:v2.0.0 \
  -f dockerfile/Dockerfile \
  context
```

* Check image: 
```bash
trivy image  \
  --severity HIGH,CRITICAL \
  --ignore-unfixed registry.gitlab.com/${PROJECT}/cowsay:v2.0.0 
```
output:
```bash
2022-12-07T23:42:42.697Z	INFO	Vulnerability scanning is enabled
2022-12-07T23:42:42.697Z	INFO	Secret scanning is enabled
2022-12-07T23:42:42.697Z	INFO	If your scanning is slow, please try '--security-checks vuln' to disable secret scanning
2022-12-07T23:42:42.697Z	INFO	Please see also https://aquasecurity.github.io/trivy/v0.35/docs/secret/scanning/#recommendation for faster secret detection
2022-12-07T23:42:42.705Z	INFO	Detected OS: debian
2022-12-07T23:42:42.705Z	INFO	Detecting Debian vulnerabilities...
2022-12-07T23:42:42.715Z	INFO	Number of language-specific files: 0

registry.gitlab.com/greenitnet/cowsay:v2.0.0 (debian 11.5)

Total: 0 (HIGH: 0, CRITICAL: 0)
```


[go to home](../../../README.md)
