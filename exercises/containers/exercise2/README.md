# Exercise 2:  Run application using cri-o runtime

## Preparation:

1. run container with cri-o as our playground
```bash
sudo podman run \
  --privileged \
  --name crio-playground \
  -h crio-playground \
  -it saschagrunert/crio-playground
```
and add bash completion for crictl:
```bash
source <(crictl completion bash)
```

2. Create sandbox
```bash
mkdir ~/crio && cd ~/crio
cat << EOF > sandbox.yml
metadata:
  name: sandbox
  namespace: default
dns_config:
  servers:
    - 8.8.8.8
EOF
```
Create sandbox pod
```bash
crictl runp sandbox.yml
```
output
```bash
...
60fbecf553e5883b5bfb252ae91e28c1718f8bd133fc5c3827f526a691f5ffa6
```

3. Export pod id to environment variable:
```bash
export POD_ID=60fbecf553e5883b5bfb252ae91e28c1718f8bd133fc5c3827f526a691f5ffa6
```


## Run application nginx

1. Pull image:
```bash 
crictl pull nginx:alpine 
```
2. create a very simple container definition in YAML:
```bash
cat << EOF > container.yml
metadata:
  name: container
image:
  image: nginx:alpine
EOF
```
3. Create container:
```bash
crictl create $POD_ID container.yml sandbox.yml
```
output:
```bash
...
a77c0bfbc0f7a4a3310ff85081f637b0fd376f0108d27f1fc36c5e0b9db07fb3
```

4. Export container id to variable:
```bash
export CONTAINER_ID=a77c0bfbc0f7a4a3310ff85081f637b0fd376f0108d27f1fc36c5e0b9db07fb3
```
5. Check containers using runc and crictl:
```bash
runc list
```
and 
```bash
crictl ps
crictl ps -a
```
6. Check process inside new container
```bash
runc ps $CONTAINER_ID
```
7. Start container:
```bash
crictl start $CONTAINER_ID
```

8. Check containers:
```bash
crictl ps
```
and
```bash
runc ps $CONTAINER_ID
```

9. Check network configuration
```bash
crictl exec $CONTAINER_ID ip addr
```
and inspect container:
```bash
crictl inspect $CONTAINER_ID
```
and pod:
```bash
crictl inspectp $POD_ID
```

10. Try to connect to application inside container:
* we will connect direct to pod ip address (taken from above):
```bash
curl -v http://172.0.0.3
```
* using port forward option:
```bash
crictl port-forward $POD_ID 80
```
output:
```
Forwarding from 127.0.0.1:80 -> 80
Forwarding from [::1]:80 -> 80
Handling connection for 80
```
and in second terminal we can run curl command (inside our playground container):
```bash
sudo podman exec -it crio-playground  /bin/bash
curl -v 127.0.0.1
```
output:
```bash
curl -v 127.0.0.1 -o /dev/null
*   Trying 127.0.0.1:80...
* TCP_NODELAY set
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0* Connected to 127.0.0.1 (127.0.0.1) port 80 (#0)
> GET / HTTP/1.1
> Host: 127.0.0.1
> User-Agent: curl/7.65.0
> Accept: */*
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Server: nginx/1.23.2
< Date: Tue, 06 Dec 2022 22:38:39 GMT
< Content-Type: text/html
< Content-Length: 615
< Last-Modified: Wed, 19 Oct 2022 10:28:53 GMT
< Connection: keep-alive
< ETag: "634fd165-267"
< Accept-Ranges: bytes
< 
{ [615 bytes data]
100   615  100   615    0     0  51250      0 --:--:-- --:--:-- --:--:-- 55909
* Connection #0 to host 127.0.0.1 left intact

```

11. Clean:

Please leave nginx container for next exercise.
Please don't close this terminal or exit from 
crio-playground container.


[go to home](../../../README.md)

[go to next](../exercise3/README.md)
