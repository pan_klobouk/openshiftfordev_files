# Exercise 1: Using runc to run container

* Create directory runc and go there:
```bash
mkdir ~/runc && cd ~/runc
```

* Create default container specification:
```bash
runc spec
cat config.json
```

* Export busybox via Docker into the rootfs directory
```bash
mkdir rootfs
docker export $(docker create busybox) | tar -C rootfs -xvf -
```

* run container:
```bash
sudo runc run mycontainerid
```

* Test container and exit (CTRL-D)
* Check list of running containers:
```bash
sudo runc list
```

* Change runtime specification to:
  * Disable terminal (container start in detached/background mode)
  * Change arg to: "sleep", "60"

```diff
+		"terminal": false,
- 		"terminal": true,

+ 			"sleep","60"
- 			"sh"
```

* Create container
```bash
sudo runc create mycontainerid
```
* Start container
```bash
sudo runc start mycontainerid
```

* Check running containers:
```bash
sudo runc list
```

* Check processes inside container:
```bash
sudo runc ps mycontainerid
```

* Run command inside container:
```bash
sudo runc exec -t mycontainerid  echo "Hello, world!"
```

* After one minute check container and delete stop container
```bash 
sudo runc list
sudo runc delete mycontainerid
```

## Check container security:

* remove process namespace from config.json file
```diff
		"namespaces": [
			{
-				"type": "pid"
-			},
-			{

				"type": "network"
			},
```

* Create and start container:
```bash 
sudo runc create mycontainerid
sudo runc start mycontainerid
```

* Check process
```bash
sudo runc ps mycontainerid
```

* Check process from container using exec
```bash
sudo runc exec -t mycontainerid  ps auxfww
```

[go to home](../../../README.md)

[go to next](../exercise2/README.md)
