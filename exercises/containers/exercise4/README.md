# Exercise 4: Debug application

1. Start Apache application
* create sandbox
```bash
cat << EOF > ~/crio/sandbox_apache.yml
metadata:
  name: sandbox_apache
  namespace: default
dns_config:
  servers:
    - 8.8.8.8
EOF
```
* pull images
```bash
crictl pull php:7.3-apache

```
* create container definition
```bash
cat << EOF > ~/crio/container_apache.yml
metadata:
  name: container_apache
image:
  image: php:7.3-apache
EOF
```
* Create sandbox pod
```bash
APACHEPOD_ID=$(crictl runp ~/crio/sandbox_apache.yml)
```
* Create container:
```bash
CONTAINERAPACHE_ID=$(crictl create $APACHEPOD_ID ~/crio/container_apache.yml ~/crio/sandbox_apache.yml)
```
* Start container:
```bash
crictl start $CONTAINERAPACHE_ID
```
2. Check application
```bash
runc ps $CONTAINERAPACHE_ID
```
```bash
crictl ps
```
```bash
crictl pods
```
```bash
crictl port-forward $APACHEPOD_ID 8080:80
```
in second terminal:
```bash
curl 127.0.0.1:8080
```
```bash
crictl logs $CONTAINERAPACHE_ID
```


3. Inspect container:
```bash
crictl inspect $CONTAINERAPACHE_ID
crictl inspectp $APACHEPOD_ID
```
4. Connect to container and fix issue
```bash
crictl exec -it $CONTAINERAPACHE_ID bash
```
Update index.html
```bash
date > index.html
echo "hello from cri-o workshop" >> index.html
```

5. Check application again
```bash
crictl port-forward $APACHEPOD_ID 8080:80
```

In second terminal:
```bash
curl 127.0.0.1:8080
```

# How to connect to container without crictl or runc tool

`nsente` - run program in different namespaces

       The nsenter command executes program in the namespace(s) that are
       specified in the command-line options (described below). If
       program is not given, then "${SHELL}" is run (default: /bin/sh).

       Enterable namespaces are:

       mount namespace
           Mounting and unmounting filesystems will not affect the rest
           of the system, except for filesystems which are explicitly
           marked as shared (with mount --make-shared; see
           /proc/self/mountinfo for the shared flag). For further
           details, see mount_namespaces(7) and the discussion of the
           CLONE_NEWNS flag in clone(2).

       UTS namespace
           Setting hostname or domainname will not affect the rest of
           the system. For further details, see uts_namespaces(7).

       IPC namespace
           The process will have an independent namespace for POSIX
           message queues as well as System V message queues, semaphore
           sets and shared memory segments. For further details, see
           ipc_namespaces(7).

       network namespace
           The process will have independent IPv4 and IPv6 stacks, IP
           routing tables, firewall rules, the /proc/net and
           /sys/class/net directory trees, sockets, etc. For further
           details, see network_namespaces(7).

       PID namespace
           Children will have a set of PID to process mappings separate
           from the nsenter process. nsenter will fork by default if
           changing the PID namespace, so that the new program and its
           children share the same PID namespace and are visible to each
           other. If --no-fork is used, the new program will be exec’ed
           without forking. For further details, see pid_namespaces(7).

       user namespace
           The process will have a distinct set of UIDs, GIDs and
           capabilities. For further details, see user_namespaces(7).

       cgroup namespace
           The process will have a virtualized view of
           /proc/self/cgroup, and new cgroup mounts will be rooted at
           the namespace cgroup root. For further details, see
           cgroup_namespaces(7).

       time namespace
           The process can have a distinct view of CLOCK_MONOTONIC
           and/or CLOCK_BOOTTIME which can be changed using
           /proc/self/timens_offsets. For further details, see
           time_namespaces(7).

## Collecting data
We need to know application ID process from container:
```bash
ps auxfww | grep apache
```
or using runc
```bash
runc ps $CONTAINERAPACHE_ID
```
From ID process we can find what namespace are used by application:
```bash
lsns -p <process id from above command/pid>
```

## Debug our application:
* Check network configuration
```bash
nsenter -t <pid> -n ip a s
```
* Check all process inside container
```bash
nsenter -t <pid> -p -r ps -ef
```
or use top command
```
nsenter -t <pid> -p -r top
```
-r - mean set the root directory for project

* Connect to all namespaces (like exec)
```bash
nsenter -t <pid> -a
```

# Clean environment

1. List all containers
```bash
crictl ps -a
```
2. List all pods
```bash
crictl pods
```
3. Stop all container/pods
```bash
crictl stop $(crictl ps -q)
crictl stopp $(crictl pods)
```
4. Remove all containers/pods
```bash
crictl rm $(crictl ps -a -q)
crictl rmp $(crictl pods)
```
5. Remove all images
```bash
crictl images
crictl rmi $(crictl images -q)
```
## Close crio-playground container

Exit interactive terminal by CTRL-D or exit or quit

[go to home](../../../README.md)

[go to next](../exercise5/README.md)
