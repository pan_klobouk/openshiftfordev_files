# Exercise 16: Docker security - compromising secrets

We will check what will happen with file with our secret when we remove it
in separate layer in Dockerfile


##  Create application with secret file

1. Go back to Cowsay application and correct Dockerfile
by adding secret file.

* Change directory:
```bash
cd ~/cowsay
```

* Create secret file:
```bash
echo "my secrets data" > secret.txt
```

2. Add secret file to application by add COPY command in Dockerfile:
```bash
cat << EOF  > dockerfiles/Dockerfile
FROM debian:bullseye-slim
LABEL maintainer="tomasz.szymanski@greenit.com.pl"
RUN apt-get update && apt-get install -y \
    cowsay \
    fortune \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*
COPY entrypoint.sh /
COPY secret.txt /opt/secret.txt
USER 1001
ENTRYPOINT ["/entrypoint.sh"]
EOF
```

3. Build application and check if you can find secret in your image:

* Build image
```bash
docker build -t test/cowsay-dockerfile -f dockerfiles/Dockerfile context
```

* Check image

    * print secret file  
```bash
docker run --rm --entrypoint /bin/cat test/cowsay-dockerfile /opt/secret.txt
```
    * check image by dive application
```bash
dive test/cowsay-dockerfile
```

4. Correct Dockerfile and remove secret file in next command
```bash
cat << EOF  > dockerfiles/Dockerfile
FROM debian:bullseye-slim
LABEL maintainer="tomasz.szymanski@greenit.com.pl"
RUN apt-get update && apt-get install -y \
    cowsay \
    fortune \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*
COPY entrypoint.sh /
COPY secret.txt /opt/secret.txt
RUN rm /opt/secret.txt
USER 1001
ENTRYPOINT ["/entrypoint.sh"]
EOF
```

5. Build image again and check secrets

* Build image
```bash
docker build -t test/cowsay-dockerfile -f dockerfiles/Dockerfile context
```

* Check image
    * print secret file  
```bash
docker run --rm --entrypoint /bin/cat test/cowsay-dockerfile /opt/secret.txt
```
    * check image by dive application
```bash
dive test/cowsay-dockerfile
```

6. Save image as tar archive and search for secret file

* Save image
```bash
docker save -o cowsay-dockerfile.tar test/cowsay-dockerfile
```

* Extract archive
```bash
mkdir extracted && tar -xf cowsay-dockerfile.tar -C extracted
```

* Find our secret file
```bash
find extracted -type d -exec echo {} \;  -exec  tar -tvf {}/layer.tar \;
```
* Extract archive with our secret

[go to home](../../../README.md)

[go to next](../exercise17/README.md)
