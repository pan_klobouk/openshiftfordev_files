# Exercise 10: Docker stats

1. Run nginx application
```bash
docker run \
  --rm \
  -d \
  -p 80:80 \
  --name test-nginx \
  nginx:1.15
```

2. Get stats from docker client:
```bash
docker stats test-nginx
```

3.  Get stats directly from docker API:
```bash
curl --unix-socket /var/run/docker.sock http:/v1.41/containers/<hash>/stats | head -n 1 | jq
```

[go to home](../../../README.md)

[go to next](../exercise11/README.md)
